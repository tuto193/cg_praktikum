#version 300 es
precision mediump float;

uniform vec2 uResolution;
uniform float time;

out vec4 out_FragColor;

///////////////////////////////////////////////////////////////////////////////
// constant variables
///////////////////////////////////////////////////////////////////////////////
const int MAX_MARCHING_STEPS = 255;
const float MIN_DIST = 0.0;
const float MAX_DIST = 100.0;
const float EPSILON = 0.0001;

///////////////////////////////////////////////////////////////////////////////
// Structs representing 3D-Objects
///////////////////////////////////////////////////////////////////////////////
// struct sphere{
//     vec3 pos;
//     float rad;
// } sphere;

// struct box{
//     vec3 pos;
//     vec3 scale;
// } box;

/**
 * Normalized direction to march in from the eye point for a single pixel.
 *
 * fieldOfView: vertical field of view in degrees
 * fragCoord: the x,y coordinate of the pixel in the output image
 */
vec3 rayDirection(float fieldOfView, vec2 fragCoord)
{
    vec2 xy = fragCoord - uResolution / 2.0;
    float z = uResolution.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}


/**
 * Signed distance function for a sphere centered at the origin with radius 1.0;
 */
float sphereSDF(vec3 samplePoint)
{
    return length(samplePoint) - 1.0;
}

/**
 * Signed distance function for a cube centered at the origin
 */
//  float cubeSDF(vec3 p, vec3 b)
//  {
//     return length(max(abs(p)-b,0.0));
//  }

 /**
 * Signed distance function for a cube centered at the origin
 * with width = height = length = 2.0
 */
float cubeSDF(vec3 p) {
    // If d.x < 0, then -1 < p.x < 1, and same logic applies to p.y, p.z
    // So if all components of d are negative, then p is inside the unit cube
    vec3 d = abs(p) - vec3(0.75, 0.75, 0.75);

    // Assuming p is inside the cube, how far is it from the surface?
    // Result will be negative or zero.
    float insideDistance = min(max(d.x, max(d.y, d.z)), 0.0);

    // Assuming p is outside the cube, how far is it from the surface?
    // Result will be positive or zero.
    float outsideDistance = length(max(d, 0.0));

    return insideDistance + outsideDistance;
}

float boxSDF(vec3 p, vec3 b){
    // If d.x < 0, then -1 < p.x < 1, and same logic applies to p.y, p.z
    // So if all components of d are negative, then p is inside the unit cube
    vec3 d = abs(p) - b;

    // Assuming p is inside the cube, how far is it from the surface?
    // Result will be negative or zero.
    float insideDistance = min(max(d.x, max(d.y, d.z)), 0.0);

    // Assuming p is outside the cube, how far is it from the surface?
    // Result will be positive or zero.
    float outsideDistance = length(max(d, 0.0));

    return insideDistance + outsideDistance;
}

/**
 * Makes a union between two Objects
 */
 float unionSDF(float a, float b)
 {
     return min(a,b);
 }

/**
 * Makes a intersection between two Objects
 */
 float intersectionSDF(float a, float b)
 {
     return max(a,b);
 }

/**
 * Subtract the second object from the first
 */
 float differenceSDF(float a, float b)
 {
     return max(a, -b);
 }

/**
 * Calculates a transformation
 */
 vec3 transform(vec3 samplePoint, vec3 trans)
 {
     return samplePoint - trans;
 }

 vec3 rotate(vec3 samplePoint, vec3 axis, float angle)
 {
     mat3 i = mat3(1.0, 0.0, 0.0,
                   0.0, 1.0, 0.0,
                   0.0, 0.0, 1.0);

     mat3 rot = mat3(axis.x * axis.x, axis.x * axis.y, axis.x * axis.z,
                     axis.y * axis.x, axis.y * axis.y, axis.y * axis.z,
                     axis.z * axis.x, axis.z * axis.y, axis.z * axis.z);

     mat3 x = mat3(0.0    , -axis.z, axis.y ,
                   axis.z , 0.0    , -axis.x,
                   -axis.y, axis.x , 0.0    );

    return (cos(angle) * i + (1.0 - cos(angle) * rot)) * samplePoint;
 }

 vec3 rotateY(vec3 samplePoint, float angle)
 {
     mat4 rotate = mat4(cos(angle), 0.0, sin(angle), 0.0,
                        0.0, 1.0, 0.0, 0.0,
                        -sin(angle), 0.0, cos(angle), 0.0,
                        0.0, 0.0, 0.0, 1.0);

     return (rotate * vec4(samplePoint, 0.0)).xyz;
 }

/**
 * Signed distance function describing the scene.
 */
float sceneSDF(vec3 samplePoint)
{
    //return sphereSDF(samplePoint);
    //return cubeSDF(rotateY(samplePoint, time/1000.0));
    //return sphereSDF(transform(samplePoint, vec3(0.0, sin(time/1000.0), 0.0)));
    return differenceSDF(sphereSDF(transform(samplePoint, vec3(0.0, sin(time/1000.0) * 1.5, 0.0))), boxSDF(rotateY(samplePoint, 45.0), vec3(0.75, 0.75, 0.75)));
}


/**
 * Raymarching algorithm, returns the distance
 * between camera and object (along the ray)
 */
float raymarching(vec3 camera, vec3 ray)
{
    float depth = 0.0;
    for (int i = 0; i < MAX_MARCHING_STEPS; i++)
    {
        float dist = sceneSDF(camera + depth * ray);
        if (dist < EPSILON)
            return depth;
        depth += dist;
        if (depth >= MAX_DIST)
            return MAX_DIST;
    }
    return MAX_DIST;
}

/**
 * TODO: estimate the normal on the surface at point p.
 */
vec3 estimateNormal(vec3 p)
{
    return normalize(vec3(sceneSDF(vec3(p.x + EPSILON, p.y, p.z)) - sceneSDF(vec3(p.x - EPSILON, p.y, p.z)),
                sceneSDF(vec3(p.x, p.y + EPSILON, p.z)) - sceneSDF(vec3(p.x, p.y - EPSILON, p.z)),
                sceneSDF(vec3(p.x, p.y, p.z + EPSILON)) - sceneSDF(vec3(p.x, p.y, p.z - EPSILON))));
}


/**
 * TODO: Lighting contribution of a single point light source via Phong illumination.
 *
 * returns RGB color of the light's contribution.
 *
 * k_a: Ambient color
 * k_d: Diffuse color
 * k_s: Specular color
 * alpha: Shininess coefficient
 * lightPos: the position of the light
 * lightIntensity: color/intensity of the light
 * camera: camera
 * p: position of point being lit
 *
 * See https://en.wikipedia.org/wiki/Phong_reflection_model#Description
 */
vec3 phongIllumination(vec3 k_a, vec3 k_d, vec3 k_s, float alpha, vec3 lightPos,
                       vec3 lightIntensity, vec3 camera, vec3 p)
{
    vec3 N = estimateNormal(p);
    vec3 V = normalize(camera - p);
    vec3 L = normalize(lightPos - p);
    vec3 R = normalize(reflect(-L, N));

    float dotLN = max(dot(L,N), 0.0);
    float dotRV = max(dot(R,V), 0.0);

    return lightIntensity * (k_a + k_d * dotLN + k_s * pow(dotRV, alpha));
}

vec3 cellShading(vec3 k_a, vec3 k_d, vec3 k_s, float alpha, vec3 lightPos,
                       vec3 lightIntensity, vec3 camera, vec3 p)
{
    vec3 N = estimateNormal(p);
    vec3 V = normalize(camera - p);
    vec3 L = normalize(lightPos - p);
    vec3 R = normalize(reflect(-L, N));

    float dotLN = max( dot(L,N), 0.0 );
    float dotRV = max( dot(R,V), 0.0 );
    // RV
    if( dotRV < 0.00001 )
    {
        dotRV = 0.0;
    }
    else if( dotRV < 0.5 )
    {
        dotRV = 0.3;
    }
    else if( dotRV < 0.9 )
    {
        dotRV = 0.5;
    }
    else
    {
        dotRV = 1.0;
    }
    // LN
     if( dotLN < 0.00001 )
    {
        dotLN = 0.0;
    }
    else if( dotLN < 0.5 )
    {
        dotLN = 0.2;
    }
    else if( dotLN < 0.9 )
    {
        dotLN = 0.5;
    }
    else
    {
        dotLN = 1.0;
    }

    return lightIntensity * (k_a + k_d * dotLN + k_s * pow(dotRV, alpha));
}

vec3 multiplePhongIllumination(vec3 k_a, vec3 k_d, vec3 k_s, float alpha, vec3 camera, vec3 p)
{
//    vec3 color = k_a * vec3(0.5, 0.5, 0.5);
    vec3 light1Pos = vec3(400.0 * sin(time/1000.0),
                          2.0,
                          400.0 * cos(time/1000.0));
    vec3 light1Intensity = vec3(0.4, 0.4, 0.4);

    vec3 color = cellShading(k_a, k_d, k_a, alpha, light1Pos, light1Intensity, camera, p);

    // vec3 light2Pos = vec3(-4.0,
    //                       2.0,
    //                       -2.0);
    // vec3 light2Intensity = vec3(0.4, 0.4, 0.4);

    // color += phongIllumination(k_a, k_d, k_s, alpha, light2Pos, light2Intensity, camera, p);
    return color;
}

mat4 viewMatrix(vec3 camera, vec3 center, vec3 up) {
	vec3 f = normalize(center - camera);
	vec3 s = normalize(cross(f, up));
	vec3 u = cross(s, f);
	return mat4(
		vec4(s, 0.0),
		vec4(u, 0.0),
		vec4(-f, 0.0),
		vec4(0.0, 0.0, 0.0, 1)
	);
}

void main()
{
    // Camera and Direction
    vec3 camera = vec3(10.0 * sin(time/800.0), 3.0 * sin(time/1500.0), 10.0 * cos(time/800.0));
    vec3 dir = rayDirection(45.0, gl_FragCoord.xy);
    mat4 view = viewMatrix(camera, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
    dir = (view * vec4(dir, 0.0)).xyz;

    // Raymarching for each Screen Pixel
    float dist = raymarching(camera, dir);

    // Ray did not hit Object
    if (dist >= MAX_DIST)
    {
        out_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
        return;
    }


    vec3 p = camera + dist * dir;

    vec3 k_a = vec3(0.2, 0.2, 0.2);
    vec3 k_d = vec3(0.7, 0.2, 0.2);
    vec3 k_s = vec3(1.0, 1.0, 1.0);
    float alpha = 10.0;
    vec3 lightPos = vec3(1000.0, 1.0, 2.0);
    vec3 lightIntensity = vec3(0.7, 0.1, 0.1);

//     vec3 color = phongIllumination(k_a, k_d, k_s, alpha, lightPos, lightIntensity, camera, p);
    vec3 color = multiplePhongIllumination(k_a, k_d, k_s, alpha, camera, p);

    out_FragColor = vec4(color, 1.0);
}
