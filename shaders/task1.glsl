#version 300 es
precision mediump float;

uniform vec2 uResolution;

out vec4 out_FragColor;

///////////////////////////////////////////////////////////////////////////////
// constant variables
///////////////////////////////////////////////////////////////////////////////
const int MAX_MARCHING_STEPS = 255;
const float MIN_DIST = 0.0;
const float MAX_DIST = 100.0;
const float EPSILON = 0.0001;

/**
 * Normalized direction to march in from the eye point for a single pixel.
 *
 * fieldOfView: vertical field of view in degrees
 * fragCoord: the x,y coordinate of the pixel in the output image
 */
vec3 rayDirection(float fieldOfView, vec2 fragCoord)
{
    vec2 xy = fragCoord - uResolution / 2.0;
    float z = uResolution.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}


/**
 * TODO: Signed distance function for a sphere centered at the origin with radius 1.0;
 */
float sphereSDF(vec3 samplePoint)
{
    return length(samplePoint) - 1.0;
}


/**
 * TODO: Raymarching algorithm, returns the distance
 * between camera and object (along the ray)
 */

float raymarching(vec3 camera, vec3 ray)
{
    float depth = MIN_DIST;
    for(int i = 0; i < MAX_MARCHING_STEPS; i++)
    {
        float dist = sphereSDF(camera + depth * ray);
        if(dist < EPSILON)
        {
            return depth;
        }
        depth += dist;
        if(depth >= MAX_DIST)
        {
            return MAX_DIST;
        }
    }
    return MAX_DIST;
}


void main()
{
    vec3 camera = vec3(0.0, 0.0, 10.0);
    vec3 dir = rayDirection(45.0, gl_FragCoord.xy);


    float dist = raymarching(camera, dir);

    /**
     * TODO: Fill red color if it hits the object
     */
    if(dist > MAX_DIST - EPSILON)
    {
        out_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
    }
    else
    {
        out_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    }
}
