#version 300 es
precision mediump float;

uniform vec2 uResolution;
uniform float time;
uniform bool mouseClicked;
uniform ivec2 deltaPos;

out vec4 out_FragColor;

///////////////////////////////////////////////////////////////////////////////
// constant variables
///////////////////////////////////////////////////////////////////////////////
const int MAX_MARCHING_STEPS = 255;
const float MIN_DIST = 0.0;
const float MAX_DIST = 100.0;
const float EPSILON = 0.0001;

///////////////////////////////////////////////////////////////////////////////
// Structs representing 3D-Objects
///////////////////////////////////////////////////////////////////////////////

/**
 * Representing surface properties of an object
 */
struct surface{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float alpha;
};

/**
 * Representing a sphere
 */
struct sphere{
    vec3 pos;
    float rad;
    surface sur;
};

/**
 * Representing a box
 */
struct box{
    vec3 pos;
    vec3 scale;
    surface sur;
};

/**
 * Representing a lightsource
 */
struct light{
    vec3 pos;
    vec3 color;
};

/**
 * Container for dist and surface properties
 */
 struct contact{
     float dist;
     surface sur;
 };


/**
 * Normalized direction to march in from the eye point for a single pixel.
 *
 * fieldOfView: vertical field of view in degrees
 * fragCoord: the x,y coordinate of the pixel in the output image
 */
vec3 rayDirection(float fieldOfView, vec2 fragCoord)
{
    vec2 xy = fragCoord - uResolution / 2.0;
    float z = uResolution.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}

/**
 * Signed distance function for a sphere centered at the origin with radius rad;
 */
float sphereSDF(sphere s, vec3 p)
{
    return length(p - s.pos) - s.rad;
}

/**
* Signed distance function for a box with particular length properties p for it's side's lengths
*/
float boxSDF(box b, vec3 p){
    // If d.x < 0, then -1 < p.x < 1, and same logic applies to p.y, p.z
    // So if all components of d are negative, then p is inside the unit cube
    vec3 d = abs(p - b.pos) - b.scale;
    
    // Assuming p is inside the cube, how far is it from the surface?
    // Result will be negative or zero.
    float insideDistance = min(max(d.x, max(d.y, d.z)), 0.0);
    
    // Assuming p is outside the cube, how far is it from the surface?
    // Result will be positive or zero.
    float outsideDistance = length(max(d, 0.0));
    
    return insideDistance + outsideDistance;
}

/**
 * Makes a union between two Objects
 */
float unionSDF(float a, float b)
{
    return min(a,b);
}

/**
 * Makes a intersection between two Objects
 */
float intersectionSDF(float a, float b)
{
    return max(a,b);
}

/**
 * Subtract the second object from the first
 */
float differenceSDF(float a, float b)
{
    return max(a, -b);
}

/**
 * Calculates a transformation
 */
vec3 transform(vec3 samplePoint, vec3 trans)
{
    return samplePoint - trans;
}

/**
* Rotate a samplePoint on an angle with respect to an axis
*/
vec3 rotate(vec3 samplePoint, vec3 axis, float angle)
{
    axis = normalize(axis);
    mat3 i = mat3(1.0, 0.0, 0.0,
                0.0, 1.0, 0.0,
                0.0, 0.0, 1.0);

    mat3 rot = mat3(axis.x * axis.x, axis.x * axis.y, axis.x * axis.z,
                    axis.y * axis.x, axis.y * axis.y, axis.y * axis.z,
                    axis.z * axis.x, axis.z * axis.y, axis.z * axis.z);

    mat3 x = mat3(0.0    , -axis.z, axis.y ,
                axis.z , 0.0    , -axis.x,
                -axis.y, axis.x , 0.0    );

    return (cos(angle) * i + (1.0 - cos(angle) * rot)) * samplePoint;
}


vec3 rotateY(vec3 samplePoint, float angle)
{
    mat4 rotate = mat4(cos(angle), 0.0, sin(angle), 0.0,
                    0.0, 1.0, 0.0, 0.0,
                    -sin(angle), 0.0, cos(angle), 0.0,
                    0.0, 0.0, 0.0, 1.0);
                    
    return (rotate * vec4(samplePoint, 0.0)).xyz;
}


/**
 * Signed distance function describing the scene.
 */
contact sceneSDF(vec3 samplePoint)
{
    // The floor surface present on the plane
    // contact floorPlane;
    // //// Surface values for the floor
    // floorPlane.sur.ambient = vec3(0.0, 0.6, 1.0);
    // floorPlane.sur.specular = vec3(0.5686, 0.0, 0.5882);
    // floorPlane.sur.diffuse = vec3(0.3333, 0.4078, 0.0);
    // floorPlane.sur.alpha = 10.0;
    // Initialize Objects
    sphere ball;
    box container;
    contact ret;

    // Set Properties
    ball.pos = vec3(0.0, 2.0 + sin(time/1000.0) * 3.0, -3.0);
    ball.rad = 1.0;
    ball.sur.ambient = vec3(0.2, 0.2, 0.2);
    ball.sur.diffuse = vec3(0.3, 0.3, 0.7);
    ball.sur.specular = vec3(1.0, 1.0, 1.0);
    ball.sur.alpha = 10.0;
    
    container.pos = vec3(0.0, 2.0, 0.0);
    container.scale = vec3(1.5, 0.75, 0.75);
    container.sur.ambient = vec3(0.2, 0.2, 0.2);
    container.sur.diffuse = vec3(0.7, 0.2, 0.2);
    container.sur.specular = vec3(1.0, 1.0, 1.0);
    container.sur.alpha = 5.0;

    float distances[2];

    // Distance to Objects
    distances[0] = sphereSDF(ball, samplePoint);
    distances[1] = boxSDF(container, rotateY(samplePoint, float(deltaPos.x)/100.0));

    int minDist = 0;
    for( int i = 1; i < 2; i++ )
    {
        if( distances[i] < distances[minDist] )
        {
            minDist = i;
        }
    }

    ret.dist = distances[minDist];
    if( minDist == 0 )
    {
        ret.sur = ball.sur;
    }
    else if( minDist == 1 )
    {
        ret.sur = container.sur;
    }
    // Return
    return ret;
}


/**
 * Raymarching algorithm, returns the distance
 * between camera and object (along the ray)
 */
contact raymarching(vec3 camera, vec3 ray)
{
    // Contact with plane
    vec3 planeP = vec3(0.0);
    vec3 planeN = vec3(0.0, 1.0, 0.0);
    float planeDist = 0.0;

    if (dot(planeN, ray) > 0.0 || camera.y < 0.0) {
        planeDist = MAX_DIST;
    } else {
        planeDist = (dot(planeP - camera, planeN)) / dot(ray, planeN);
    }

    // Initilize Raymarch
    float depth = 0.0;
    contact ret;
    ret.dist = MAX_DIST;

    // Cast a ray and check for collision
    for (int i = 0; i < MAX_MARCHING_STEPS; i++)
    {
        // Distance to nearest Object
        contact dist = sceneSDF(camera + depth * ray);
        if (dist.dist < EPSILON){
            ret.dist = depth;
            ret.sur = dist.sur;
            return ret;
        }
        
        depth += min(dist.dist, planeDist - depth);
        if (depth >= MAX_DIST)
        {
            return ret;
        }

        if(depth > planeDist - EPSILON)
        {
            // Floor surface
            ret.sur.ambient = vec3(0.2, 0.2, 8.0);
            ret.sur.specular = vec3(0.5686, 0.0, 0.5882);
            ret.sur.diffuse = vec3(0.3333, 0.4078, 0.0);
            ret.sur.alpha = 100.0;
            ret.dist = planeDist;
            return ret;
        }
    }
    return ret;
}

/**
 * Checks if ther is an Object between a lightsource and a point
 */
float shadow(vec3 p, vec3 light, float k)
{
    float depth = 0.0;
    float res = 1.0;
    float max_dist = length(p - light);
    vec3 ray = normalize(p - light);
    for(float t = 0.0; t < max_dist - EPSILON * 1000.0;)
    {
        float dist = sceneSDF(light + depth * ray).dist;

        depth += dist;
        if(dist < EPSILON)
        {
            return 0.0;
        }

        res = min(res, k*dist/t);
        t += dist;
    }
    return res;
}

/**
 * TODO: estimate the normal on the surface at point p.
 */
vec3 estimateNormal(vec3 p)
{
    return normalize(vec3(sceneSDF(vec3(p.x + EPSILON, p.y, p.z)).dist - sceneSDF(vec3(p.x - EPSILON, p.y, p.z)).dist,
                sceneSDF(vec3(p.x, p.y + EPSILON, p.z)).dist - sceneSDF(vec3(p.x, p.y - EPSILON, p.z)).dist,
                sceneSDF(vec3(p.x, p.y, p.z + EPSILON)).dist - sceneSDF(vec3(p.x, p.y, p.z - EPSILON)).dist));
}


/**
 * TODO: Lighting contribution of a single point light source via Phong illumination.
 *
 * returns RGB color of the light's contribution.
 *
 * k_a: Ambient color
 * k_d: Diffuse color
 * k_s: Specular color
 * alpha: specular coefficient
 * lightPos: the position of the light
 * lightIntensity: color/intensity of the light
 * camera: camera
 * p: position of point being lit
 *
 * See https://en.wikipedia.org/wiki/Phong_reflection_model#Description
 */
vec3 phongIllumination(surface sur, vec3 lightPos, vec3 lightIntensity, vec3 camera, vec3 p)
{
    vec3 N;
    if(p.y >= -EPSILON*10.0 && p.y <= EPSILON*10.0)
    {
        N = vec3(0.0, 1.0, 0.0);
    }
    else
    {
        N = estimateNormal(p);
    }
    vec3 V = normalize(camera - p);
    vec3 L = normalize(lightPos - p);
    vec3 R = normalize(reflect(-L, N));

    float dotLN = max(dot(L,N), 0.0);
    float dotRV = max(dot(R,V), 0.0);

    float shadow = shadow(p, lightPos, 3000.0);

    return lightIntensity * (sur.ambient + shadow * (sur.diffuse * dotLN + sur.specular * pow(dotRV, sur.alpha)));
}

vec3 cellShading(surface sur, vec3 lightPos, vec3 lightIntensity, vec3 camera, vec3 p)
{
    vec3 N = estimateNormal(p);
    vec3 V = normalize(camera - p);
    vec3 L = normalize(lightPos - p);
    vec3 R = normalize(reflect(-L, N));

    float dotLN = max( dot(L,N), 0.0 );
    float dotRV = max( dot(R,V), 0.0 );
    // RV
    if( dotRV < 0.00001 )
    {
        dotRV = 0.0;
    }
    else if( dotRV < 0.5 )
    {
        dotRV = 0.3;
    }
    else if( dotRV < 0.9 )
    {
        dotRV = 0.5;
    }
    else
    {
        dotRV = 1.0;
    }
    // LN
     if( dotLN < 0.00001 )
    {
        dotLN = 0.0;
    }
    else if( dotLN < 0.5 )
    {
        dotLN = 0.2;
    }
    else if( dotLN < 0.9 )
    {
        dotLN = 0.5;
    }
    else
    {
        dotLN = 1.0;
    }

    return lightIntensity * (sur.ambient + sur.diffuse * dotLN + sur.specular * pow(dotRV, sur.alpha));
}


vec3 multiplePhongIllumination(surface sur, vec3 camera, vec3 p)
{
    vec3 light1Pos = vec3(0.0,
                          8.0,
                          10.0);
    vec3 light1Intensity = vec3(0.4, 0.4, 0.4);

    vec3 color = phongIllumination(sur, light1Pos, light1Intensity, camera, p);

    return color;
}

mat4 viewMatrix(vec3 camera, vec3 center, vec3 up) {
	vec3 f = normalize(center - camera);
	vec3 s = normalize(cross(f, up));
	vec3 u = cross(s, f);
	return mat4(
		vec4(s, 0.0),
		vec4(u, 0.0),
		vec4(-f, 0.0),
		vec4(0.0, 0.0, 0.0, 1)
	);
}

void main()
{
    // Camera and Direction
    vec3 camera = vec3(10.0 /* * sin(time/800.0) */,5.0 /* + 3.0 * sin(time/1500.0) */, 10.0 /* * cos(time/800.0) */);
    vec3 dir = rayDirection(45.0, gl_FragCoord.xy);
    mat4 view = viewMatrix(camera, vec3(0.0, 2.0, 0.0), vec3(0.0, 1.0, 0.0));
    dir = (view * vec4(dir, 0.0)).xyz;

    // Raymarching for each Screen Pixel
    contact dist = raymarching(camera, dir);

    // Ray did not hit Object
    if (dist.dist >= MAX_DIST)
    {
        out_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
        return;
    }


    vec3 p = camera + dist.dist * dir;

    vec3 color = multiplePhongIllumination(dist.sur, camera, p);

    out_FragColor = vec4(color, 1.0);
}
