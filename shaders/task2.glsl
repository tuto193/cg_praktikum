#version 300 es
precision mediump float;

uniform vec2 uResolution;

out vec4 out_FragColor;

///////////////////////////////////////////////////////////////////////////////
// constant variables
///////////////////////////////////////////////////////////////////////////////
const int MAX_MARCHING_STEPS = 255;
const float MIN_DIST = 0.0;
const float MAX_DIST = 100.0;
const float EPSILON = 0.0001;


/**
 * Normalized direction to march in from the eye point for a single pixel.
 *
 * fieldOfView: vertical field of view in degrees
 * fragCoord: the x,y coordinate of the pixel in the output image
 */
vec3 rayDirection(float fieldOfView, vec2 fragCoord)
{
    vec2 xy = fragCoord - uResolution / 2.0;
    float z = uResolution.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}


/**
 * Signed distance function for a sphere centered at the origin with radius 1.0;
 */
float sphereSDF(vec3 samplePoint)
{
    return length(samplePoint) - 1.0;
}


/**
 * Signed distance function describing the scene.
 */
float sceneSDF(vec3 samplePoint)
{
    return sphereSDF(samplePoint);
}


/**
 * Raymarching algorithm, returns the distance
 * between camera and object (along the ray)
 */
float raymarching(vec3 camera, vec3 ray)
{
    float depth = 0.0;
    for (int i = 0; i < MAX_MARCHING_STEPS; i++)
    {
        float dist = sceneSDF(camera + depth * ray);
        if (dist < EPSILON)
            return depth;
        depth += dist;
        if (depth >= MAX_DIST)
            return MAX_DIST;
    }
    return MAX_DIST;
}

/**
 * TODO: estimate the normal on the surface at point p.
 */
vec3 estimateNormal(vec3 p)
{
    return normalize(vec3(sceneSDF(vec3(p.x + EPSILON, p.y, p.z)) - sceneSDF(vec3(p.x - EPSILON, p.y, p.z)),
                sceneSDF(vec3(p.x, p.y + EPSILON, p.z)) - sceneSDF(vec3(p.x, p.y - EPSILON, p.z)),
                sceneSDF(vec3(p.x, p.y, p.z + EPSILON)) - sceneSDF(vec3(p.x, p.y, p.z - EPSILON))));
}


/**
 * TODO: Lighting contribution of a single point light source via Phong illumination.
 *
 * returns RGB color of the light's contribution.
 *
 * k_a: Ambient color
 * k_d: Diffuse color
 * k_s: Specular color
 * alpha: Shininess coefficient
 * lightPos: the position of the light
 * lightIntensity: color/intensity of the light
 * camera: camera
 * p: position of point being lit
 *
 * See https://en.wikipedia.org/wiki/Phong_reflection_model#Description
 */
vec3 phongIllumination(vec3 k_a, vec3 k_d, vec3 k_s, float alpha, vec3 lightPos,
                       vec3 lightIntensity, vec3 camera, vec3 p)
{
    vec3 N = estimateNormal(p);
    vec3 V = normalize(camera - p);
    vec3 L = normalize(lightPos - p);
    vec3 R = normalize(reflect(-L, N));

    float dotLN = dot(L,N);
    float dotRV = dot(R,V);

    if(dotLN < 0.0)
    {
        if(dotRV < 0.0)
        {
            return lightIntensity * (k_a);
        }
        return lightIntensity * (k_a + k_s * pow(dotRV, alpha));
    }

    if(dotRV < 0.0)
    {
        return lightIntensity * (k_a + k_d * dotLN);
    }
    return lightIntensity * (k_a + k_d * dotLN + k_s * pow(dotRV, alpha));
}


vec3 multiplePhongIllumination(vec3 k_a, vec3 k_d, vec3 k_s, float alpha, vec3 camera, vec3 p)
{
//    vec3 color = k_a * vec3(0.5, 0.5, 0.5);
    vec3 light1Pos = vec3(4.0,
                          2.0,
                          4.0);
    vec3 light1Intensity = vec3(0.4, 0.4, 0.4);

    vec3 color = phongIllumination(k_a, k_d, k_s, alpha, light1Pos, light1Intensity, camera, p);

    vec3 light2Pos = vec3(-4.0,
                          2.0,
                          -2.0);
    vec3 light2Intensity = vec3(0.4, 0.4, 0.4);

    color += phongIllumination(k_a, k_d, k_s, alpha, light2Pos, light2Intensity, camera, p);
    return color;
}

void main()
{
    vec3 camera = vec3(0.0, 0.0, 10.0);
    vec3 dir = rayDirection(45.0, gl_FragCoord.xy);

    float dist = raymarching(camera, dir);

    if (dist >= MAX_DIST)
    {
        out_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
        return;
    }

    /*
     * TODO: illuminate the surface point with phongIllumination
     */
    vec3 p = camera + dist * dir;

    vec3 k_a = vec3(0.2, 0.2, 0.2);
    vec3 k_d = vec3(0.7, 0.2, 0.2);
    vec3 k_s = vec3(1.0, 1.0, 1.0);
    float alpha = 10.0;
    vec3 lightPos = vec3(1000.0, 1.0, 2.0);
    vec3 lightIntensity = vec3(0.7, 0.1, 0.1);

//     vec3 color = phongIllumination(k_a, k_d, k_s, alpha, lightPos, lightIntensity, camera, p);
    vec3 color = multiplePhongIllumination(k_a, k_d, k_s, alpha, camera, p);

    out_FragColor = vec4(color, 1.0);
}
