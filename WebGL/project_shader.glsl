#version 300 es
precision mediump float;

uniform vec2 uResolution;
uniform float time;
uniform float uDisplacementSpeed;
uniform bool mouseClicked;
uniform ivec2 deltaPos;
uniform vec3 uCamPos;
uniform vec3 uCamDir;
uniform vec3 uCamUp;
uniform vec3 uFloorColor;

// Dice Information
uniform vec3 uDiceRot;
uniform vec3 uDicePos;
uniform int uDiceNumber;
/**
 * Use the mode on the dice, to determine the effects that will take place in the scene
 * 1: plane is gone
 * 2: water-like
 * 3: cell shading
 * 4: transparency + subsurfabs
 * 5: reflection
 * 6: rainbow
 */

out vec4 out_FragColor;

///////////////////////////////////////////////////////////////////////////////
// constant variables
///////////////////////////////////////////////////////////////////////////////
const int MAX_MARCHING_STEPS = 255;
const float MIN_DIST = 0.01;
const float MAX_DIST = 100.0;
const float EPSILON = 0.001;
const vec3 LIGHT_POS = vec3( 0.0, 1000.0, 0.0 );
const vec3 LIGHT_INT = vec3( 0.4, 0.4, 0.4 );
const float PI = 3.14159265;

/**
 * Representing surface properties of an object
 */
struct surface{
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float alpha;
    // [0, 1]
    float opacity;
    float translucency;
    // [0, 2]
    float distortion;
    float reflection;
};
/**
 * Representing a sphere
 */
struct sphere{
    vec3 pos;
    float rad;
    surface sur;
};

/**
 * Representing a box
 */
struct box
{
    vec3 pos;
    vec3 scale;
    surface sur;
};

/**
 * Representing a lightsource
 */
struct light
{
    vec3 pos;
    vec3 color;
};

/**
 * Container for dist and surface properties
 */
struct contact{
    float dist;
    int id;
};


//// Funciton declarations, so we don't have to worry about the order in which we use the methods

vec3 rayDirection(float fieldOfView, vec2 fragCoord);
float sphereSDF(vec3 pos, float rad, vec3 p);
float boxSDF(vec3 pos, vec3 dim, vec3 p);
float pipSDF(float rad, vec3 p);
float waterSDF(vec3 p);
float displaceSDF(vec3 p);
float floorIntersection(vec3 camera, vec3 ray);
float unionSDF(float a, float b);
float intersectionSDF(float a, float b);
float differenceSDF(float a, float b);
vec3 translate(vec3 samplePoint, vec3 trans);
vec3 rotate(vec3 samplePoint, vec3 axis, float angle);
vec3 rotateY(vec3 samplePoint, float angle);
vec3 rotateX(vec3 samplePoint, float angle);
vec3 rotateZ(vec3 samplePoint, float angle);
surface mapSurface(int objectID);
float mapSDF(int objectID, vec3 p);
contact sceneSDF(vec3 samplePoint);
contact raymarching(vec3 camera, vec3 ray);
float shadow(vec3 p, vec3 light, float k);
float shadowThroughOpacity(vec3 p, vec3 light, float k);
vec3 estimateNormal(vec3 p, int object);
float hash( float n );
float calculateDepth( int objectID, vec3 p, vec3 n, float maxDist, float fallOff );
vec3 calculateAbsorption( int objID,  vec3 aColor, vec3 lightIntensity, float depth );
vec3 calculateAbsorption( int objID, vec3 vEye, vec3 p, vec3 vNormal, vec3 vLight );
vec3 saturate( vec3 surCol, float density );
vec2 scatterLighting( surface sur, float NdotL, float NdotH, float scattW );
vec3 phongIllumination(int objectID, vec3 lightPos, vec3 lightIntensity, vec3 camera, vec3 p);
vec3 cellShading(int objectID, vec3 lightPos, vec3 lightIntensity, vec3 camera, vec3 p);
vec2 cellShading( float dotRV, float dotLN );
vec3 multiplePhongIllumination(int object, vec3 camera, vec3 p);
mat4 viewMatrix(vec3 camera, vec3 center, vec3 up);
float voronoi( vec2 x, float u, float v );
float smoothVoronoi( vec2 x );
vec3 hash3( vec2 p );
float groundHeight (vec2 p);
float groundRay (vec3 ro, vec3 rd);
vec3 groundNormal (vec3 p);
vec3 estimateNormal(vec3 p, int object);
float kkkNoise( vec3 p );
// Constant surfaces
// solidOpaqueBlue
const surface solidOpaqueBlue = surface(vec3(0.2, 0.2, 0.2),            //ambient
                                        vec3(0.5216, 0.6471, 0.9216),   //diffuse
                                        vec3(0.2863, 0.2863, 0.2863),   //specular
                                        10.0,                           //alpha
                                        1.0,                            //opacity
                                        0.7,                             //translucency
                                        1.0,
                                        0.0);

// semi-transparent-shiny-blue
const surface semiTransShinyBlue = surface( vec3(0.0, 0.1451, 0.8),
                                            vec3(0.0, 0.4431, 0.702),
                                            vec3(0.8941, 0.8941, 0.8941),
                                            40.0,
                                            0.5,
                                            0.7,
                                            0.5,
                                            0.0);

// solidShinyGreen
const surface solidShinyGreen = surface(vec3(0.1529, 0.7608, 0.0),
                                        vec3(0.6902, 0.8471, 0.6706),
                                        vec3(1.0, 1.0, 1.0),
                                        100.0,
                                        1.0,
                                        0.0,
                                        0.5,
                                        0.0);

// solidRoughRed
const surface solidRoughRed = surface(vec3(1.0, 0.0, 0.0),
                                      vec3(0.6549, 0.5137, 0.5137),
                                      vec3(1.0, 1.0, 1.0),
                                      5.0,
                                      1.0,
                                      0.0,
                                      1.0,
                                      0.0);

// halfTransparentRed
const surface halfTransparentRed = surface(vec3(1.0, 0.0, 0.0),
                                          vec3(0.6549, 0.5137, 0.5137),
                                          vec3(0.4588, 0.4588, 0.4588),
                                          40.0,
                                          .6,
                                          0.8,
                                          2.,
                                          .3);




const surface defaultSurface = surface(vec3(1.0, 1.0, 1.0),
                                       vec3(1.0, 1.0, 1.0),
                                       vec3(1.0, 1.0, 1.0),
                                       3.0,
                                       1.0,
                                       0.0,
                                       1.0,
                                       0.0);

// blackMirror
const surface blackMirror = surface(vec3(0.0, 0.0, 0.0),                //ambient
                                    vec3(0.0, 0.0, 0.0),                //diffuse
                                    vec3(0.0, 0.0, 0.0),                //specular
                                    10.0,                             //alpha
                                    1.0,                              //opacity
                                    0.0,                              //translucency
                                    1.0,
                                    1.0);                             //reflection

// waterLike
const surface waterLike = surface(vec3(0.0235, 0.0824, 0.2392),                //ambient
                                    vec3(0.0549, 0.302, 0.4471),                //diffuse
                                    vec3(0.0353, 0.3451, 0.1647),                //specular
                                    2.0,                             //alpha
                                    0.8,                              //opacity
                                    0.0,                              //translucency
                                    1.0,
                                    0.1);                             //reflection


/**
 * Normalized direction to march in from the eye point for a single pixel.
 *
 * fieldOfView: vertical field of view in degrees
 * fragCoord: the x,y coordinate of the pixel in the output imagesdf3d
 */
vec3 rayDirection(float fieldOfView, vec2 fragCoord)
{
    vec2 xy = fragCoord - uResolution / 2.0;
    float z = uResolution.y / tan(radians(fieldOfView) / 2.0);
    return normalize(vec3(xy, -z));
}

/**
 * Signed distance function for a sphere centered at the origin with radius rad;
 */
float sphereSDF(vec3 pos, float rad, vec3 p)
{
    return length(p - pos) - rad;
}

/**
 * Signed distance function for a box with particular length properties p for it's side's lengths
 */
float boxSDF(vec3 pos, vec3 dim, float r, vec3 p)
{
    vec3 q = abs(p - pos) - dim;
    return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0) - r;
}

float pipSDF(float rad, vec3 p)
{
    return differenceSDF(sphereSDF(vec3(0.0, 0.0, 0.0), rad, p), boxSDF(vec3(0.0, rad + 0.01, 0.0), vec3(rad + 0.1, rad, rad + 0.1), 0.0, p));
}

float waterSDF(vec3 p)
{
    float d2 = //(smoothVoronoi(p.xy) * smoothVoronoi(p.yz) * smoothVoronoi(p.xz) ) * sin(time/6000.)*0.05;//
    cos(sin(time/6000.0)*15.0*p.x)*sin(cos(time/6000.0)*15.*p.y)*sin(cos(time/6000.0)*23.*p.z)*cos(30.1) * 0.05;
    float d1 = sphereSDF(vec3(0.0, 2.0, 0.0), 1.0, p);
    return d1 + d2;
}

float displaceSDF(vec3 p)
{
    return ((cos(sin(time/6000.0)*15.0*p.x)*sin(cos(time/6000.0)*15.*p.y)*sin(cos(time/6000.0)*23.*p.z))*cos(30.1)) * 0.05;
}

/**
 * Calculates the intersection with the Floor
 */
float floorIntersection(vec3 camera, vec3 ray)
{
    // Contact with plane
    vec3 planeP = vec3(0.0, 0.0, 0.0);
    vec3 planeN = vec3(0.0, 1.0, 0.0);

    if (dot(planeN, ray) > 0.0 || camera.y < 0.0) {
        return MAX_DIST;
    } else {
        return (dot(planeP - camera, planeN)) / dot(ray, planeN);
    }
}

/**
 * Makes a union between two Objects
 */
float unionSDF(float a, float b)
{
    return min(a,b);
}

/**
 * Makes a intersection between two Objects
 */
float intersectionSDF(float a, float b)
{
    return max(a,b);
}

/**
 * Subtract the second object from the first
 */
float differenceSDF(float a, float b)
{
    return max(a, -b);
}

/**
 * Calculates a transformation
 */
vec3 translate(vec3 samplePoint, vec3 trans)
{
    return samplePoint - trans;
}

/**
 * Rotate a samplePoint on an angle with respect to an axis
 */
vec3 rotate(vec3 samplePoint, vec3 axis, float angle)
{
    axis = normalize(axis);
    mat3 i = mat3(1.0, 0.0, 0.0,
                0.0, 1.0, 0.0,
                0.0, 0.0, 1.0);

    mat3 rot = mat3(axis.x * axis.x, axis.x * axis.y, axis.x * axis.z,
                    axis.y * axis.x, axis.y * axis.y, axis.y * axis.z,
                    axis.z * axis.x, axis.z * axis.y, axis.z * axis.z);

    mat3 x = mat3(0.0    , -axis.z, axis.y ,
                axis.z , 0.0    , -axis.x,
                -axis.y, axis.x , 0.0    );

    return (cos(angle) * i + (1.0 - cos(angle) * rot)) * samplePoint;
}

/**
 * Rotate a sample point at an angle but only on it's y axis (along the x-z plane)
 */
vec3 rotateY(vec3 samplePoint, float angle)
{
    mat4 rotate = mat4(cos(angle),  0.0, sin(angle), 0.0,
                       0.0,         1.0, 0.0,        0.0,
                       -sin(angle), 0.0, cos(angle), 0.0,
                       0.0,         0.0, 0.0,        1.0);

    return (rotate * vec4(samplePoint, 0.0)).xyz;
}

vec3 rotateX(vec3 samplePoint, float angle)
{
    mat4 rotate = mat4(1.0, 0.0,        0.0,         0.0,
                       0.0, cos(angle), -sin(angle), 0.0,
                       0.0, sin(angle), cos(angle),  0.0,
                       0.0, 0.0,        0.0,         1.0);

    return (rotate * vec4(samplePoint, 0.0)).xyz;
}

vec3 rotateZ(vec3 samplePoint, float angle)
{
    mat4 rotate = mat4(cos(angle), -sin(angle), 0.0, 0.0,
                       sin(angle), cos(angle),  0.0, 0.0,
                       0.0,        0.0,         1.0, 0.0,
                       0.0,        0.0,         0.0, 1.0);

    return (rotate * vec4(samplePoint, 0.0)).xyz;
}

/**
 * Maps a given ObjectID to its surface
 */
surface mapSurface(int object)
{
    surface solidShinyRainbow = surface( vec3( sin(time/6000.), cos( time/600.), -cos(time/600.)),
                                         vec3( -sin(time/990.), cos( time/4200.), cos( time/1230.) ),
                                         vec3( cos( time/500.), sin( time/5000.), cos( time/4200.) ),
                                         100., // alpha
                                         1.0,  // opacity
                                         1.0,  // translucency
                                         0.,   // distortion
                                         0. );// reflection
    surface surfaces[3];
    surface diceSurfaces[7];
    diceSurfaces[0] = defaultSurface; // normal
    diceSurfaces[1] = defaultSurface; // plane loss
    diceSurfaces[2] = waterLike; // water
    diceSurfaces[3] = solidRoughRed; // cellShad
    diceSurfaces[4] = halfTransparentRed; // transp
    diceSurfaces[5] = blackMirror; // ref
    diceSurfaces[6] = solidShinyRainbow;
    /// Floor, dice and pips
    surfaces[0] = defaultSurface;
    surfaces[1] = diceSurfaces[uDiceNumber];
    surfaces[2] = solidOpaqueBlue;
    return surfaces[object + 1];
}

/**
* Maps a given Object to its SDF
*/
float mapSDF(int object, vec3 p)
{
    if(object < 0 || object > 1) return MAX_DIST;
    float pips[21];

    // Size of the whole dice with pips
    float diceSize = 0.2;

    // Position of the whole dice with pips
    vec3 dicePos = translate(p, uDicePos);

    // Rotation of the whole dice with pips
    dicePos = rotateX(dicePos, uDiceRot.x);
    dicePos = rotateY(dicePos, uDiceRot.y);
    dicePos = rotateZ(dicePos, uDiceRot.z);

    // Dice
    float dice = boxSDF(vec3(0.0, 0.0, 0.0), vec3(diceSize, diceSize, diceSize), 0.1, dicePos);
    if(uDiceNumber == 2) dice += displaceSDF(dicePos);
    if(object == 0) return dice;
    // COMMENT ME OUT TO SEE THE PIPS!
    return MAX_DIST;

    // if(dice > 0.5) return MAX_DIST;
    // Pips
    float rad = diceSize/4.0;

    vec3 transSurface = vec3(0.0, diceSize + 0.1, 0.0);

    vec3 rotSide = rotateX(dicePos, PI/2.0);

    vec3 side1 = translate(dicePos, transSurface);
    vec3 side2 = translate(rotSide, transSurface);
    vec3 side3 = translate(rotateZ(rotSide, PI/2.0), transSurface);
    vec3 side4 = translate(rotateZ(rotSide, -PI/2.0), transSurface);
    vec3 side5 = translate(rotateZ(rotSide, PI), transSurface);
    vec3 side6 = translate(rotateX(dicePos, PI), transSurface);

    vec3 bottomright = vec3(rad*2.5, 0.0, rad*2.5);
    vec3 topleft = -bottomright;

    vec3 topright = vec3(rad*2.5, 0.0, -rad*2.5);
    vec3 bottomleft = -topright;

    vec3 right = vec3(rad*2.5, 0.0, 0.0);
    vec3 left = -right;

    // 1
    pips[0] = pipSDF(rad, side1);

    // 2
    pips[1] = pipSDF(rad, translate(side2, topleft));
    pips[2] = pipSDF(rad, translate(side2, bottomright));

    // 3
    pips[3] = pipSDF(rad, side3);
    pips[4] = pipSDF(rad, translate(side3, topleft));
    pips[5] = pipSDF(rad, translate(side3, bottomright));

    // 4
    pips[6] = pipSDF(rad, translate(side4, topleft));
    pips[7] = pipSDF(rad, translate(side4, topright));
    pips[8] = pipSDF(rad, translate(side4, bottomleft));
    pips[9] = pipSDF(rad, translate(side4, bottomright));

    // 5
    pips[10] = pipSDF(rad, side5);
    pips[11] = pipSDF(rad, translate(side5, topleft));
    pips[12] = pipSDF(rad, translate(side5, topright));
    pips[13] = pipSDF(rad, translate(side5, bottomleft));
    pips[14] = pipSDF(rad, translate(side5, bottomright));

    // 6
    pips[15] = pipSDF(rad, translate(side6, topleft));
    pips[16] = pipSDF(rad, translate(side6, topright));
    pips[17] = pipSDF(rad, translate(side6, left));
    pips[18] = pipSDF(rad, translate(side6, right));
    pips[19] = pipSDF(rad, translate(side6, bottomleft));
    pips[20] = pipSDF(rad, translate(side6, bottomright));

    float allPips = MAX_DIST;
    for(int i = 0; i < 21; i++)
    {
        allPips = unionSDF(allPips, pips[i]);
    }

    return allPips;
}

/**
 * Signed distance function describing the scene.
 */
contact sceneSDF(vec3 samplePoint)
{
    // Iterates through all Objects
    float minDist = abs(mapSDF(0, samplePoint));
    int minDistID = 0;
    for(int i = 1; i < 2; i++)
    {
        float dist = abs(mapSDF(i, samplePoint));
        if(dist < minDist)
        {
            minDist = dist;
            minDistID = i;
        }
    }
    // Returns the one with minimum distance
    return contact(minDist, minDistID);
}

contact sceneSDFabs(vec3 samplePoint)
{
    // Iterates through all Objects
    float minDist = abs(mapSDF(0, samplePoint));
    int minDistID = 0;
    for(int i = 1; i < 4; i++)
    {
        float dist = abs(mapSDF(i, samplePoint));
        if(dist < minDist)
        {
            minDist = dist;
            minDistID = i;
        }
    }
    // Returns the one with minimum distance
    return contact(minDist, minDistID);
}

/**
 * Raymarching algorithm, returns the distance
 * between camera and object (along the ray)
 */
contact raymarching(vec3 camera, vec3 ray)
{
    // Contact with plane
    float planeDist = //floorIntersection(camera, ray);//
    groundRay(camera, ray);

    // Initialize Raymarch
    float depth = MIN_DIST;
    contact ret;
    ret.dist = MAX_DIST;

    // Cast a ray and check for collision
    for (int i = 0; i < MAX_MARCHING_STEPS; i++)
    {
        // Distance to nearest Object
        contact dist = sceneSDF(camera + depth * ray);
        if (dist.dist < EPSILON){
            ret.dist = depth;
            ret.id = dist.id;
            return ret;
        }

        depth += min(dist.dist, planeDist - depth);

        if (depth >= MAX_DIST)
        {
            return ret;
        }

        if(depth > planeDist - EPSILON)
        {
            ret.dist = planeDist;
            ret.id = -1;
            return ret;
        }
    }
    return ret;
}

/**
 * Calculate soft-shadow on Surface Point
 */
float shadow(vec3 p, vec3 light, float k)
{
    float res = 1.0;
    float max_dist = length(p - light);
    vec3 ray = normalize(p - light);
    for(float t = 0.0; t < max_dist - EPSILON*10.0;)
    {
        float dist = sceneSDF(light + t * ray).dist;

        if(dist < EPSILON)
        {
            return 0.0;
        }

        t += dist;

        if(t < max_dist - EPSILON*10.0)
            res = min(res, k*dist/(max_dist-t));
    }
    return res;
}

/**
 * Get soft-shadow value with opacity in mind
 * p the point that is to be shaded
 * light the origin point of the light casted
 * k the strength of the shadows to be casted (the higher, the thicker)
 */
float shadowThroughOpacity(vec3 p, vec3 light, float k)
{
    float res = 1.0;
    float max_dist = length(p - light);
    vec3 ray = normalize(p - light);
    int hit = -1;
    for(float t = 0.0; t < max_dist - 0.1;)
    {
        contact dist = sceneSDFabs(light + t * ray);
        float opacity = mapSurface(dist.id).opacity;
        t += dist.dist;

        if(t < max_dist - 0.1)
        {
            res = min(res, (1.0 - opacity) + ((k * dist.dist) / (max_dist - t)));
        }

        if(dist.dist < EPSILON)
        {
            t += EPSILON;
        }

        if(res < EPSILON)
        {
            return 0.0;
        }
    }
    return res;
}

/**
 * TODO: estimate the normal on the surface at point p.
 */
vec3 estimateNormal(vec3 p, int object)
{
    return normalize(
        vec3(mapSDF(object, vec3(p.x + EPSILON, p.y, p.z)) - mapSDF(object, vec3(p.x - EPSILON, p.y, p.z)),
        mapSDF(object, vec3(p.x, p.y + EPSILON, p.z)) - mapSDF(object, vec3(p.x, p.y - EPSILON, p.z)),
        mapSDF(object, vec3(p.x, p.y, p.z + EPSILON)) - mapSDF(object, vec3(p.x, p.y, p.z - EPSILON))));
}

/**
 * Assign a random value between 0 and 1 to a number
 */
float hash( float n )
{
    return fract( sin(n) * 2342.2346 );
}

/**
 * Calculate the depth travelled by light, that went in through an entry point pIn and came
 * out an exit point pOut and looks into the camera cam
 * returns float depth travelled inside the object
 */
float calculateDepth( int objectID, vec3 p, vec3 n, float maxDist, float fallOff )
{
    // the number of iterations
    const int nbIte = 6;
    const float nbIteInv = 1.0/float(nbIte);
    float ao = 0.0;
    for( int i = 0; i < nbIte; i++ )
    {
        float l = hash( float( i ) ) * maxDist;
        vec3 rd = normalize( -n ) * l;
        ao += (l + mapSDF( objectID, p + rd )) / pow( 1.0 + l, fallOff );
    }
    return clamp( 1.0 - ao*nbIteInv, 0.0, 1.0 );
}

/**
 * Calculates the resulting color, that comes out after a light ray passes through a material
 *
 */
vec3 calculateAbsorption( int objID, vec3 aColor, vec3 lightIntensity, float depth )
{
    // Hier it's important to note, the translucency of the material the light came in through
    surface sur = mapSurface( objID );
    return exp( -depth * sur.translucency ) * aColor * lightIntensity;
}

vec3 calculateAbsorption( int objID, vec3 vEye, vec3 p, vec3 vNormal, vec3 vLight )
{
    surface sur = mapSurface( objID );
    float fLightAttenuation = dot(vNormal, vLight) * dot( -vLight, vEye );
    // the higher, the more the light will get absorbed
    float iLTPower = (1.0 - sur.translucency) * 12.0;
    // The bigger, the brighterlight will shine through
    float fLTScale = 1.6;
    vec3 vLTLight = vLight + vNormal * sur.distortion;
    float fLTDot = pow( clamp( dot( vEye, -vLTLight ), 0.0, 1.0 ), iLTPower) * fLTScale;
    vec3 fLT = fLightAttenuation * ( fLTDot + sur.ambient ) * calculateDepth( objID, p, vNormal, 4.0, 2.0 );
    vec3 absCol = fLT * sur.diffuse * LIGHT_INT;
    absCol = clamp( absCol, 0.0, 1.0 );
    return absCol;
}

vec3 saturate( vec3 surCol, float density )
{
    return vec3( clamp( surCol - surCol*density, 0.0, 1.0) );
}

/**
 * calculate the amount of light scattered on a surface sur, based on a Normal N,
 * light source L and Halfway-Vector H, and a width for the scatterW IN [0, 1]
 * returns a vec2( specFactor, scatter)
 */
vec2 scatterLighting( surface sur, float NdotL, float NdotH, float scattW )
{
    // clip the values of NL and NH from [0, 1] to [-1, 1]
    NdotL = NdotL * 2.0 - 1.0;
    NdotH = NdotH * 2.0 - 1.0;
    // wrap lighting
    float NdotL_wrap = (NdotL + sur.translucency) / (1.0 + sur.translucency);
    // The final diffuse factor
    NdotL = max(NdotL_wrap, 0.0);
    // this is the strength of the color transition from light to dark
    float scatter = smoothstep(0.0, scattW, NdotL_wrap) * smoothstep(scattW* 2.0, scattW, NdotL_wrap);

    float specularFactor = max( pow( NdotH, sur.alpha ), 0.0 );
    return vec2( specularFactor, scatter );
}

/**
 * Use the values from Phong Shading and clip their values
 * returns a vec2( NEWdotRV, NEWdotLN )
 */
vec2 cellShading( float dotRV, float dotLN )
{
    // RV
    if( dotRV < 0.00001 )
    {
        dotRV = 0.0;
    }
    else if( dotRV < 0.5 )
    {
        dotRV = 0.3;
    }
    else if( dotRV < 0.9 )
    {
        dotRV = 0.5;
    }
    else
    {
        dotRV = 1.0;
    }
    // LN
     if( dotLN < 0.00001 )
    {
        dotLN = 0.0;
    }
    else if( dotLN < 0.5 )
    {
        dotLN = 0.2;
    }
    else if( dotLN < 0.9 )
    {
        dotLN = 0.5;
    }
    else
    {
        dotLN = 1.0;
    }

    return vec2( dotRV, dotLN );
}

/**
 * TODO: Lighting contribution of a single point light source via Phong illumination.
 *
 * returns RGB color of the light's contribution.
 *
 * k_a: Ambient color
 * k_d: Diffuse color
 * k_s: Specular color
 * alpha: specular coefficient
 * lightPos: the position of the light
 * lightIntensity: color/intensity of the light
 * camera: camera
 * p: position of point being lit
 *
 * See https://en.wikipedia.org/wiki/Phong_reflection_model#Description
 */
vec3 phongIllumination(int object, vec3 lightPos, vec3 lightIntensity, vec3 camera, vec3 p)
{
    surface sur;
    vec3 N;
    N = estimateNormal(p, object);
    sur = mapSurface(object);
    vec3 V = normalize(camera - p);
    vec3 L = normalize(lightPos - p);
    vec3 H = normalize( L + V );
    float scatterWidth = 0.7;
    float shader = 1.0;
    vec3 diffuseC;
    vec3 specC;
    vec2 lightScattered = vec2( 0.0 );
    vec3 absorbedColor = vec3(0.0);
    // Hitting the floor which is not in sceneSDF
    if(object == -1)
    {
        // N = vec3(0.0, 1.0, 0.0);
        N = groundNormal( p );
        // Floor pattern
        vec2 xz = (p).xz;
        if(xz.x < 0.0)
            xz.x -= 5.0;
        if(xz.y < 0.0)
            xz.y -= 5.0;
        float col = float((int(xz.x/5.0) + int(xz.y/5.0)) % 2);

        // Floor surface
        vec3 compliment = vec3(1.) - uFloorColor;
        vec3 colors[2];
        colors[0] = normalize(compliment);
        colors[1] = uFloorColor;
        sur.ambient = colors[int(col)];
    }
    vec3 R = normalize(reflect(-L, N));
    vec3 scatterColor = sur.ambient * 0.5;
    float dotLN = max(dot(L,N), 0.0);
    float dotRV = max(dot(R,V), 0.0);
//    out_FragColor = vec4(R, 1.0);
    // We will just cast shadows, if the L key hasn't been toggled
    shader = shadowThroughOpacity(p, lightPos, 100.0);
    lightScattered.x = pow( dotRV, sur.alpha );
    if( uDiceNumber == 3 )
    {
        lightScattered = cellShading( dotRV, dotLN );

        diffuseC = sur.diffuse * dotLN * shader + lightScattered.y * scatterColor;
        specC = sur.specular * lightScattered.x * shader;
        return lightIntensity * (sur.ambient + diffuseC + specC ) + absorbedColor;
    }

    // Scatter lighting happens inside this block////////////
    float NdotH = dot(N, H);
    NdotH = NdotH < 0.0? 0.0: NdotH;
    float newNdotL = dotLN < -0.1? -0.1: dotLN;
    lightScattered = scatterLighting( sur, newNdotL, NdotH, scatterWidth );
    absorbedColor = calculateAbsorption( object, V, p, N, L);
    //////////////////////////////////////////////////////////
    diffuseC = sur.diffuse * dotLN * shader + lightScattered.y * scatterColor;
    specC = sur.specular * lightScattered.x * shader;
    diffuseC = max(diffuseC, vec3(0.0));
    specC = max(specC, vec3(0.0));
    return lightIntensity * (sur.ambient + diffuseC + specC ) + absorbedColor;
}

/**
 * Use phong shading, but clip the values of the specular and diffuse, so that a comic-like lighting is achieved
 */
vec3 cellShading(int object, vec3 lightPos, vec3 lightIntensity, vec3 camera, vec3 p)
{
    surface sur;
    vec3 N;

    // Hitting the floor which is not in sceneSDF
    if(object == -1)
    {
        N = vec3(0.0, 1.0, 0.0);
        // Floor pattern
        vec2 xz = (p).xz;
        if(xz.x < 0.0)
            xz.x -= 1.0;
        if(xz.y < 0.0)
            xz.y -= 1.0;
        float col = float((int(xz.x) + int(xz.y)) % 2);

        // Floor surface
        sur.ambient = vec3(1.0 * col, 1.0 * col, 1.0 * col);
        sur.specular = vec3(1.0, 1.0, 1.0);
        sur.diffuse = vec3(1.0, 1.0, 1.0);
        sur.alpha = 100.0;
        sur.opacity = 1.0;
    }
    else
    {
        N = estimateNormal(p, object);
        sur = mapSurface(object);
    }

    vec3 V = normalize(camera - p);
    vec3 L = normalize(lightPos - p);
    vec3 R = normalize(reflect(-L, N));

    float dotLN = max( dot(L,N), 0.0 );
    float dotRV = max( dot(R,V), 0.0 );
    // RV
    if( dotRV < 0.00001 )
    {
        dotRV = 0.0;
    }
    else if( dotRV < 0.5 )
    {
        dotRV = 0.3;
    }
    else if( dotRV < 0.9 )
    {
        dotRV = 0.5;
    }
    else
    {
        dotRV = 1.0;
    }
    // LN
     if( dotLN < 0.00001 )
    {
        dotLN = 0.0;
    }
    else if( dotLN < 0.5 )
    {
        dotLN = 0.2;
    }
    else if( dotLN < 0.9 )
    {
        dotLN = 0.5;
    }
    else
    {
        dotLN = 1.0;
    }

    return lightIntensity * (sur.ambient + sur.diffuse * dotLN + sur.specular * pow(dotRV, sur.alpha));
}

/**
 * Use two light sources and add the resulting values of their illumination to the point being lit
 */
vec3 multiplePhongIllumination(int object, vec3 camera, vec3 p)
{
    vec3 light1Pos = vec3(uCamPos.x, uCamPos.y + 10.0, uCamPos.z - 5.0);
    vec3 light1Intensity = LIGHT_INT;

    vec3 color = phongIllumination(object, light1Pos, light1Intensity, camera, p);

    return color;
}

mat4 viewMatrix(vec3 camera, vec3 center, vec3 up) {
	vec3 f = normalize(center - camera);
	vec3 s = normalize(cross(f, up));
	vec3 u = cross(s, f);
	return mat4(
		vec4(s, 0.0),
		vec4(u, 0.0),
		vec4(-f, 0.0),
		vec4(0.0, 0.0, 0.0, 1)
	);
}

vec3 hash3( vec2 p )
{
    p  = 50.0*fract( p*0.3183099 );
    return vec3( p, fract( p.x*p.y*(p.x+p.y) ) );
}

float voronoi( vec2 x, float u, float v )
{
    // the grid-postion
    vec2 p = floor(x);
    // the fractional remainder
    vec2 f = fract(x);

    float k = 1.0 + 63.0*pow(1.0-v,4.0);
    float va = 0.0;
    float wt = 0.0;
    for( int j=-2; j<=2; j++ )
    for( int i=-2; i<=2; i++ )
    {
        // grid position
        vec2  g = vec2( float(i), float(j) );
        vec3  o = hash3( p + g )*vec3(u,u,1.0);
        vec2  r = g - f + o.xy;
        float d = dot(r,r);
        float w = pow( 1.0-smoothstep(0.0,1.414,sqrt(d)), k );
        va += w*o.z;
        wt += w;
    }

    return va/wt;
}

float smoothVoronoi( vec2 x )
{
    vec2 p = floor( x );
    vec2  f = fract( x );

    float res = 0.0;
    for( int j=-1; j<=1; j++ )
    for( int i=-1; i<=1; i++ )
    {
        vec2 b = vec2( i, j );
        vec2  r = vec2( b ) - f + hash3( p + b ).yz;
        float d = length( r );

        res += exp( -6.9*d );
    }
    return -(1.0/32.0)*log( res );
}

// ground height
float groundHeight (vec2 p)
{
    mat2 fqRot;
    vec2 q;
    float h, a;
    fqRot = 1. * mat2 (0.6, -0.8, 0.8, 0.6);
    q = 0.03 * p;
    h = 0.;
    a = 10.;
    for (int j = 0; j < 5; j ++) {
        //
        h += a * kkkNoise(vec3(q, 1.0)) * uDisplacementSpeed;
        a *= 0.5;
        q *= fqRot;
    }
    return h;
}

// ground ray
float groundRay (vec3 ro, vec3 rd)
{
    if( uDiceNumber == 1 ) { return MAX_DIST; }
    vec3 point;
    float distanceHit, height, s, sLo, sHi;
    s = 0.;
    sLo = 0.;
    distanceHit = MAX_DIST;
    for (int j = 0; j < 160; j ++) {
        point = ro + s * rd;
        height = point.y - groundHeight(point.xz);
        if (height < 0.) break;
        sLo = s;
        s += max (0.5, 0.5 * height);
        if (s > MAX_DIST) break;
    }
    if (height < 0.) {
        sHi = s;
        for (int j = 0; j < 5; j ++) {
        s = 0.5 * (sLo + sHi);
        point = ro + s * rd;
        if (point.y > groundHeight (point.xz)) sLo = s;
        else sHi = s;
        }
        distanceHit = 0.5 * (sLo + sHi);
    }
    return distanceHit;
}

// ground normal
vec3 groundNormal (vec3 p)
{
  vec2 e;
  e = vec2 (0.01, 0.);
  return normalize (vec3 (groundHeight (p.xz) - vec2 (groundHeight (p.xz + e.xy), groundHeight (p.xz + e.yx)), e.x).xzy);
}

float kkkNoise( vec3 p )
{
    float h = 0.5;
    return cos(sin(h)*15.*p.x)*sin(cos(h)*15.*p.y)*sin(cos(h)*23.*p.z)*cos(30.1) * h;
}
void main()
{
    vec3 dir = rayDirection(45.0,  gl_FragCoord.xy);
    mat4 view = viewMatrix(uCamPos, uCamDir, uCamUp);
    dir = (view * vec4(dir, 0.0)).xyz;

    vec3 rayPoint = uCamPos;
    vec3 color = vec3(0.0);
    // Raymarching for each Screen Pixel

    // Loop for hitting Opject
    for(float r = 1.0; r > EPSILON;)
    {
        contact dist = raymarching(rayPoint, dir);
        // Ray did not hit Object
        if (dist.dist >= MAX_DIST)
        {
            r = 0.0;
        }
        else
        {
            surface sur = mapSurface(dist.id);
            float reflection = sur.reflection;
            vec3 p = rayPoint + dist.dist * dir;
            // Reflection
            if(reflection > EPSILON)
            {
                vec3 dirR = normalize(reflect(dir, estimateNormal(p, dist.id)));
                vec3 rayPointR = p;
                // First Reflection Loop
                for(float rr = r * sur.reflection; rr > EPSILON;)
                {
                    contact distR = raymarching(rayPointR, dirR);
                    // Ray did not hit Object
                    if (distR.dist >= MAX_DIST)
                    {
                        rr = 0.0;
                    }
                    else
                    {
                        surface surR = mapSurface(distR.id);
                        vec3 pR = rayPointR + distR.dist * dirR;
                        float opaqR = rr * surR.opacity;
                        color += phongIllumination(distR.id, vec3(uCamPos.x, uCamPos.y + 10.0, uCamPos.z - 5.0), LIGHT_INT, rayPointR, pR) * opaqR *(min(1.0, (1.0-dist.dist/MAX_DIST) * 20.0));
                        rayPointR = pR;
                        rr -= opaqR;
                    }

                }
            }
            float opaq = r * sur.opacity;
            color += phongIllumination(dist.id, vec3(uCamPos.x, uCamPos.y + 10.0, uCamPos.z - 5.0), LIGHT_INT, rayPoint, p) * opaq * (1.0 - reflection) * (min(1.0, (1.0-dist.dist/MAX_DIST) * 20.0));
            rayPoint = p;
            r -= opaq;
        }
    }
    out_FragColor = vec4(color, 1.0);
}
