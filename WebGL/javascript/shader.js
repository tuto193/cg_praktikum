/**
 * Set a number a to stay within an interval I = [b, c]
 * @param {number} a the number to be clamped
 * @param {number} b the lowest bound
 * @param {number} c the highest bound
 */
var clamp = function( a, b, c )
{
    return a < b? b: a > c?c:a;
}

/**
 * Get the fractional part of a number f (the first 4 numbers after the coma)
 * @param {number} f  the number who's fraction we want
 */
var fract = function( f ) {
    return Math.ceil(((f < 1.0) ? f : (f % Math.floor(f))) * 10000);
}

// noise
var kkkNoise = function (pX, pY, pZ) {
    var h = 0.5;
    return Math.cos(Math.sin(h)*15.*pX)*Math.sin(Math.cos(h)*15.*pY)*Math.sin(Math.cos(h)*23.*pZ)*Math.cos(30.1) * h;
}

var smoothVoronoi = function( x = new Vector3D ){
    var p = new Vector3D( Math.floor( x.x ), Math.floor(x.y), 0.0 );
    var f = new Vector3D( fract( x.x ), fract(x.y), 0.0 );

    var res = 0.0;
    for( var j=-1; j<=1; j++ )
    for( var i=-1; i<=1; i++ )
    {
        var b = new Vector3D( i, j, 0.0 );
        var pNb = p.add(b);
        var r = b.add( f.mul(-1))
        var noise = kkkNoise( pNb.x, pNb.y, pNb.z );
        r = new Vector3D( r.y+noise, r.z+noise, 0.0);
        var d = r.len();

        res += Math.exp( -6.9*d );
    }
    return -(1.0/32.0)*Math.log( res );
}

///// Point Vector Matrix Methods /////
// 3dpoint prototype
function Point3D (x=0, y=0, z=0, w=1)  {
    this.x = x;
    this.y = y;
    this.z = z;
    this.w = w;

    // dehomogenize point
    this.dehomogen = function () {
        var p = new Point3D();
        p.x = this.x/this.w;
        p.y = this.y/this.w;
        p.z = this.z/this.w;
        p.w = 1;
        return p;
    }

    // subtract other point from this point and return it as vector
    this.subtract = function (pIn) {
        var v = new Vector3D();
        v.x = this.x - pIn.x;
        v.y = this.y - pIn.y;
        v.z = this.z - pIn.z;
        return v;
    }
}

// 3d vector
function Vector3D (x=0, y=0, z=0)  {
    this.x = x;
    this.y = y;
    this.z = z;

    // matrix vector product
    this.len = function() {
        return Math.sqrt( this.x*this.x + this.y*this.y + this.z*this.z );
    }

    this.cross = function (v) {
        var c = new Vector3D();
        c.x = this.y * v.z - this.z * v.y;
        c.y = this.z * v.x - this.x * v.z;
        c.z = this.x * v.y - this.y * v.x;
        return c;
    }

    this.clipValues = function()
    {
        this.x = clamp( this.x, 0.0, 1.0 );
        this.y = clamp( this.y, 0.0, 1.0 );
        this.z = clamp( this.z, 0.0, 1.0 );
    }

    // normalize vector
    this.normalize = function() {
        var norm = Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
        var v = new Vector3D(this.x/norm, this.y/norm, this.z/norm);
        return v;
    }

    // dot product
    this.dot = function (v) {
        return this.x * v.x + this.y * v.y + this.z * v.z;
    }

    // multiplication by scalar
    this.mul = function (s) {
        return new Vector3D(this.x * s, this.y * s, this.z * s);
    }

    // multiplication by vector (component-wise)
    this.mulVec = function (v) {
        return new Vector3D(this.x * v.x, this.y * v.y, this.z * v.z);
    }

    // add
    this.add = function (vIn) {
        return new Vector3D(this.x + vIn.x, this.y + vIn.y, this.z + vIn.z);
    }

    // subtract
    this.sub = function (vIn) {
        return new Vector3D(this.x - vIn.x, this.y - vIn.y, this.z - vIn.z);
    }

    this.reflect = function(nIn) {
        return this.add( nIn.mul(-2.0).mul( this.dot(nIn)) );
    }
}

// 4d matrix protoype
function Matrix4D (xx = 1, yx = 0, zx = 0, wx = 0,
                    xy = 0, yy = 1, zy = 0, wy = 0,
                    xz = 0, yz = 0, zz = 1, wz = 0,
                    xw = 0, yw = 0, zw = 0, ww = 1) {
    this.xx = xx;
    this.yx = yx;
    this.zx = zx;
    this.wx = wx;
    this.xy = xy;
    this.yy = yy;
    this.zy = zy;
    this.wy = wy;
    this.xz = xz;
    this.yz = yz;
    this.zz = zz;
    this.wz = wz;
    this.xw = xw;
    this.yw = yw;
    this.zw = zw;
    this.ww = ww;
    // matrix addition
    this.addMat = function( m ) {
        var n = new Matrix4D ();
        n.xx = m.xx + this.xx;
        n.xy = this.xy + m.xy;
        n.xz = this.xz + m.xz;
        n.xw = this.xw + m.xw;
        n.yx = this.yx + m.yx;
        n.yy = this.yy + m.yy;
        n.yz = this.yz + m.yz;
        n.yw = this.yw + m.yw;
        n.zx = this.zx + m.zx;
        n.zy = this.zy + m.zy;
        n.zz = this.zz + m.zz;
        n.zw = this.zw + m.zw;
        n.wx = this.wx + m.wx;
        n.wy = this.wy + m.wy;
        n.wz = this.wz + m.wz;
        n.ww = this.ww + m.ww;
        return n;
    }
    // matrix multiplication
    this.mulMat = function (m) {
        var n = new Matrix4D ();
        n.xx = m.xx * this.xx + m.xy * this.yx + m.xz * this.zx + m.xw * this.wx;
        n.xy = m.xx * this.xy + m.xy * this.yy + m.xz * this.zy + m.xw * this.wy;
        n.xz = m.xx * this.xz + m.xy * this.yz + m.xz * this.zz + m.xw * this.wz;
        n.xw = m.xx * this.xw + m.xy * this.yw + m.xz * this.zw + m.xw * this.ww;
        n.yx = m.yx * this.xx + m.yy * this.yx + m.yz * this.zx + m.yw * this.wx;
        n.yy = m.yx * this.xy + m.yy * this.yy + m.yz * this.zy + m.yw * this.wy;
        n.yz = m.yx * this.xz + m.yy * this.yz + m.yz * this.zz + m.yw * this.wz;
        n.yw = m.yx * this.xw + m.yy * this.yw + m.yz * this.zw + m.yw * this.ww;
        n.zx = m.zx * this.xx + m.zy * this.yx + m.zz * this.zx + m.zw * this.wx;
        n.zy = m.zx * this.xy + m.zy * this.yy + m.zz * this.zy + m.zw * this.wy;
        n.zz = m.zx * this.xz + m.zy * this.yz + m.zz * this.zz + m.zw * this.wz;
        n.zw = m.zx * this.xw + m.zy * this.yw + m.zz * this.zw + m.zw * this.ww;
        n.wx = m.wx * this.xx + m.wy * this.yx + m.wz * this.zx + m.ww * this.wx;
        n.wy = m.wx * this.xy + m.wy * this.yy + m.wz * this.zy + m.ww * this.wy;
        n.wz = m.wx * this.xz + m.wy * this.yz + m.wz * this.zz + m.ww * this.wz;
        n.ww = m.wx * this.xw + m.wy * this.yw + m.wz * this.zw + m.ww * this.ww;
        return n;
    }

    // matrix vector product
    this.mulVec = function (pIn) {
        var p = new Point3D();
        p.x = pIn.x * this.xx + pIn.y * this.yx + pIn.z * this.zx + pIn.w * this.wx;
        p.y = pIn.x * this.xy + pIn.y * this.yy + pIn.z * this.zy + pIn.w * this.wy;
        p.z = pIn.x * this.xz + pIn.y * this.yz + pIn.z * this.zz + pIn.w * this.wz;
        p.w = pIn.x * this.xw + pIn.y * this.yw + pIn.z * this.zw + pIn.w * this.ww;
        return p;
    }

    // matrix multiplication with a scalar
    this.mulScal = function( scal ) {
        var n = new Matrix4D ();
        n.xx = this.xx * scal;
        n.xy = this.xy * scal;
        n.xz = this.xz * scal;
        n.xw = this.xw * scal;
        n.yx = this.xx * scal;
        n.yy = this.xy * scal;
        n.yz = this.xz * scal;
        n.yw = this.xw * scal;
        n.zx = this.xx * scal;
        n.zy = this.xy * scal;
        n.zz = this.xz * scal;
        n.zw = this.xw * scal;
        n.wx = this.xx * scal;
        n.wy = this.xy * scal;
        n.wz = this.xz * scal;
        n.ww = this.xw * scal;
        return n;
    }
}

/* ** * @class
 * Simple fps counter that displays the current fps in an HTML element
 */
class FPSCounter
{
    /**
     * @constructor
     * Creates a new fps counter instance
     *
     * @param {string} id id of the HTML element to write to
     * @param {number} limit number of frames that should be averaged
     */
    constructor(id, limit=30) {
        this.values = [];
        this.limit = limit;
        this.prev = 0;
        this.index = 0;
        this.elemId = id;
    }

    /**
     * Updates the counter by adding the new frame time induced by the timestamp
     *
     * @param {number} timestamp frame time since 'origin'
     */
    update(timestamp) {
        if (!this.prev) {
            this.prev = timestamp;
        }

        const duration = timestamp - this.prev;
        if (this.values.length < this.limit) {
            this.values.push(duration);
        } else {
            this.values[this.index] = duration;
            this.index = (this.index + 1) % this.limit;
        }

        this.prev = timestamp;

        document.getElementById(this.elemId).innerHTML = Math.floor(this.fps);
    }

    /**
     * @returns {number} current frames per second (averaged)
     */
    get fps() {
        return 1000 / (this.values.reduce((sum, val) => sum + val, 0) / this.values.length);
    }
}

/**
 * @class
 * Representing the floor
 */
class floor
{
    constructor()
    {
        // floor displacement and speed of it
        // this.displaceFloor = false;
        // this.displacementSpeed = 0.0;
        this.displacement = 0.0;
        this.displace = false;
        this.color1 = new Vector3D( 1.0, 1.0, 1.0 );
    }



    // ground height
    groundHeight (xPos, zPos, d = this.displacement)
    {
        var qX, qZ;
        var h, a;
        var fqRot11 = 0.6;
        var fqRot12 = -0.8;
        var fqRot21 = 0.8;
        var fqRot22 = 0.6;

        qX = 0.03 * xPos;
        qZ = 0.03 * zPos;
        h = 0.0;
        a = 10.0;
        for (var j = 0; j < 5; j ++) {
            //
            h += a * kkkNoise(qX, qZ, 1.0) * d;
            a *= 0.5;
            var dX = qX;
            qX = fqRot11 * dX + qZ * fqRot12;
            qZ = fqRot21 * dX + qZ * fqRot22;
        }
        return h;
    }

    // ground normal
    groundNormal (p = new Vector3D())
    {
    var e = 0.01;
    return new Vector3D(this.groundHeight(p.x, p.z) - this.groundHeight(p.x + e, p.z),
                        e,
                        this.groundHeight(p.x, p.z) - this.groundHeight(p.x, p.z + e)).normalize();
    //return normalize (vec3 (groundHeight (p.x, p.z) - vec2 (groundHeight (p.x + e, p.z), groundHeight (p.x, p.z + e)), e.x).xzy);
    }

    displaceFloor(d = 0.2)
    {
        this.displacement = Math.min(this.displacement + d, 1.0);
    }

    unDisplaceFloor(d = 0.2)
    {
        this.displacement = Math.max(this.displacement - d, 0.0);
    }

    setDisplace(b)
    {
        this.displace = b;
    }

    step()
    {
        if(this.displace)
            this.displaceFloor(0.2);
        else
            this.unDisplaceFloor(0.2);
    }


}

/**
 * @class
 * Representing a dice
 */
class dice
{
    constructor(f = new floor())
    {
        // Dimension and Position
        this.size = 0.2;
        this.pos = new Vector3D(0.0, this.size + 0.15, 0.0);

        // Rotation
        this.rot = new Vector3D(0.0, 0.0, 0.0);
        this.rotSpeed = new Vector3D(0.0, 0.0, 0.0);

        // Speed And Accel
        // this.dir = new Vector3D(0.0, 1.0, 0.0);
        this.speed = new Vector3D(0.0, 0.0, 0.0);
        // this.maxHeight = 0.0;

        // Dice Number
        this.diceNumber = 1;
        this.spinX = 0.0;
        this.spinZ = 0.0;

        // Steps
        this.steps = 0;
        this.stopping = false;
        this.starting = false;

        // Grnd hits left
        this.ghl = 3;

        // Floor
        this.floor = f;

        this.roll = false;

        this.stopTimer = 0.0;
        this.startTimer = 0.0;

        // this.yHeightMul = 0.0;

        this.camPos = new Vector3D(this.pos.x + 4, this.pos.y * 1.3 + 5, this.pos.z + 4);
        this.camDir = this.pos;

        this.actualDiceNumber = 0;


    }

    rollNumber(n = 0)
    {
        if(n > 0 && n < 7)
        {
            this.diceNumber = n;
        }
        else
        {
            this.diceNumber = Math.floor(Math.random() * 6 + 1);
        }
        console.log("Rolled: " + this.diceNumber);
        switch(this.diceNumber)
        {
            case 1:
                this.spinX = 0.0;
                this.spinZ = 0.0;
                break;
            case 2:
                this.spinX = -Math.PI/2.0;
                this.spinZ = 0.0
                break;
            case 3:
                this.spinX = 0.0;
                this.spinZ = -Math.PI/2.0;
                break;
            case 4:
                this.spinX = 0.0;
                this.spinZ = Math.PI/2.0;
                break;
            case 5:
                this.spinX = Math.PI/2.0;
                this.spinZ = 0.0
                break;
            case 6:
                this.spinX = Math.PI;
                this.spinZ = 0.0
                break;

        }
    }

    hitGrnd()
    {
        return ((this.pos.y - this.size * 2.0) < (this.floor.groundHeight(this.pos.x, this.pos.z)));
    }

    randomDir()
    {
        this.speed = new Vector3D(Math.random()*2-1, Math.random(), Math.random()*2-1).normalize().mul(0.1);
        // this.maxHeight = this.speed.y;
    }

    randomRot()
    {
        this.rotSpeed = new Vector3D(Math.random()*2-1, 0.0, Math.random()*2-1).mul(0.2);
        this.rotSpeed = this.rotSpeed.mul( 1 + this.speed.len() );
    }

    actSpeedAndRot()
    {
        // Actualize Pos and Rot
        this.pos = this.pos.add(this.speed);
        this.rot = this.rot.add(this.rotSpeed);
        // Actualize Speed and Rotation Speed
        // console.log("Hit Ground: " + this.hitGrnd());
        if(this.hitGrnd())
        {
            if(this.stopTimer > 0.4)
            {
                var x = Math.random();
                var y = Math.random();
                var z = Math.random();
                var newD = new Vector3D( x, y, z );
                var newDx = kkkNoise( newD.x, newD.y, newD.z);
                var newDy = kkkNoise( newD.x, newD.z, newD.y);
                var newDz = kkkNoise( newD.z, newD.x, newD.y);
                this.floor.color1 = new Vector3D( newDx, newDy, newDz).normalize()
            }
            this.randomRot();
            this.speed = this.speed.reflect(this.floor.groundNormal(this.pos));
            this.randomRot();
            // this.speed = new Vector3D(this.speed.x, -this.speed.y, this.speed.z);
            // this.randomDir();
            // this.maxHeight = this.speed.y;
        }
        else
        {
            this.speed.y -= 0.01;

        }
        this.rotSpeed = this.rotSpeed.mul(Math.pow(this.stopTimer, 0.2));

        if(this.rot.x > Math.PI) this.rot.x -= 2 * Math.PI;
        if(this.rot.x < -Math.PI) this.rot.x += 2 * Math.PI;
        if(this.rot.y > Math.PI) this.rot.y -= 2 * Math.PI;
        if(this.rot.y < -Math.PI) this.rot.y += 2 * Math.PI;
        if(this.rot.z > Math.PI) this.rot.z -= 2 * Math.PI;
        if(this.rot.z < -Math.PI) this.rot.z += 2 * Math.PI;

        // console.log(Math.sin(this.stopTimer * Math.PI / 2.0));
        var lol = Math.pow(this.stopTimer, 0.01);
        this.speed = this.speed.mul(lol);
        this.pos.y = this.pos.y * lol + (1 - lol) * (this.size + 0.1);
        this.rot.x = this.rot.x * lol + (1 - lol) * this.spinX;
        this.rot.y = this.rot.y * lol;
        this.rot.z = this.rot.z * lol + (1 - lol) * this.spinZ;
    }

    start()
    {
        this.speed.x = 0;
        this.speed.z = 0;
        this.speed.y = 1.0;
        this.pos.y = 2;
        this.randomRot();
        this.roll = true;
        this.stopping = false;
        this.starting = true;
        this.startTimer = 0.0;
        this.stopTimer = 1.0;
        this.floor.setDisplace(true);
    }

    stop(n = 0)
    {
        this.rollNumber(n);
        this.stopping = true;
        this.starting = false;
        this.floor.setDisplace(false);
    }

    step()
    {
        console.log(this.stopping);
        if(this.stopping)
        {
            this.stopTimer = Math.max(0.0, this.stopTimer - 0.003);
            if(this.stopTimer < 0.0001 && this.roll)
            {
                this.roll = false
                setTimeout( ()=>{
                    this.actualDiceNumber = this.diceNumber;
                    if(this.actualDiceNumber == 1)
                    {
                        this.randomRot();
                    }
                    console.log("Actual Number set to: " + this.actualDiceNumber );
                }, 500);
            }
        }
        else if( this.starting ){
            this.startTimer = Math.min( 1.0, this.startTimer + 0.009);
        }
        this.actSpeedAndRot();

        var lol = Math.min(this.stopTimer, this.startTimer);
        this.camPos.x = this.pos.x + 11 * lol + 4;
        this.camPos.y = (this.pos.y * 1.3 + 5);
        this.camPos.z = this.pos.z + 4 + 9 * lol;

        this.camDir = this.pos;
    }

    fall()
    {
        this.speed.y = Math.max(this.speed.y - 0.03, -0.5);
        this.pos.y += this.speed.y;
        this.rot = this.rot.add(this.rotSpeed);
        if( this.pos.y < -120 ){
            this.rot.x = this.spinX ;
            this.rot.y =  0;
            this.rot.z = this.spinZ ;
            this.pos.y = this.size + 0.1;
            this.actualDiceNumber = 0;
        }
    }
}

const DEFAULT_WIDTH = 640;
const DEFAULT_HEIGHT = 400;
const startCamPos = new Vector3D( 20, 10, 20 );
const startCamDir = new Vector3D( 0, 4, 0 );
const startCamUp = new Vector3D( 0, 1, 0 );


/**
 * @class
 * A renderer that manages a WebGL2 context and renders a fullscreen quad
 */
class Renderer
{

    /**
     * @constructor
     * Creates a new renderer instance (also calls addEventListener())
     *
     * @param {string} canvasName id of the canvas element
     * @param {string} vertSrc vertex shader source
     * @param {string} fragSrc fragment shader source
     */
    constructor(canvasName, vertSrc, fragSrc) {
        this.vertSrc = vertSrc;
        this.fragSrc = fragSrc;
        this.canvas = window.document.getElementById(canvasName);
        // ids and attribute locations
        this.progID = 0;
        this.vertID = 0;
        this.fragID = 0;
        this.bufID = 0;
        this.vertexLoc = 0;
        this.color1Loc = 0;
        // Camera Values
        this.cameraPos = new Vector3D( 10, 10, 10 );
        this.cameraDir = new Vector3D( 0, 4, 0 );
        this.cameraUp = new Vector3D( 0, 1, 0 );
        // mouse propertys for shader
        this.canvasMouseX = 0;
        this.canvasMouseY = 0;
        this.canvasMouseMoveX = 0;
        this.canvasMouseMoveY = 0;
        this.canvasClicked = false;
        // WASD for moving
        this.canvasPressW = false;
        this.canvasPressA = false;
        this.canvasPressS = false;
        this.canvasPressD = false;
        // JKLI for looking
        this.canvasPressJ = false;
        this.canvasPressK = false;
        this.canvasPressL = false;
        this.canvasPressI = false;
        // 0-9 to turn on/off visual effects
        this.canvasPress7 = false;
        this.canvasPress8 = false;
        this.canvasPress9 = false;
        // floor
        this.floor = new floor();
        // dice number
        this.dice = new dice(this.floor);
        this.rolling = false;
        // uniform locations
        this.uniforms = {};
        // color defines
        this.clearColor = [0, 0, 0, 1];
        // misc
        this.fps = new FPSCounter("fps");
        this.parent = document.getElementById("container");

        this.addEventListener();
    }

    /**
     * Print the camera attributes to the console for debugging purposes
     */
    printCameraAttributes() {
        // Print the position:
        console.log("Camera is at: ");
        console.log(this.cameraPos);
        console.log("Camera is lookint at:" );
        console.log(this.cameraDir);
        console.log("Camera's current UP is:");
        console.log(this.cameraUp);

    }

    /**
     * Adds the event listeners needed for the fullscreen functionality
     */
    addEventListener() {
        // Event checking for pressed keys
        document.addEventListener("keypress", (event) => {
            if (event.key == "f") {
                this.toggleFullscreen();
            }
            else {
                this.toggleButtonEvent(event.key);
                // this.printCameraAttributes();
            }
        });

        // Event checking for letting go of keys
        document.addEventListener("keyup", (e) => {
            switch( e.key ) {
                case "w":
                    this.canvasPressW = false;
                    break;
                case "a":
                    this.canvasPressA = false;
                    break;
                case "s":
                    this.canvasPressS = false;
                    break;
                case "d":
                    this.canvasPressD = false;
                    break;
                case "j":
                    this.canvasPressJ = false;
                    break;
                case "k":
                    this.canvasPressK = false;
                    break;
                case "l":
                    this.canvasPressL = false;
                    break;
                case "i":
                    this.canvasPressI = false;
                    break;
                default:
                    break;
            }
        } );
        this.parent.addEventListener("fullscreenchange", () => {
            if (!document.fullscreenElement) {
                this.resize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
            } else {
                this.resize();
            }
        });
        document.getElementById("go-full")
            .addEventListener("click", this.toggleFullscreen.bind(this));

        // Event checking for mouse actions
        document.getElementById("m_Canvas").addEventListener("mousedown", () => {
            this.canvasClicked = true;
        });
        document.getElementById("m_Canvas").addEventListener("mousemove", (e) => {
            if(this.canvasClicked) {
                var canvasDeltaX = e.clientX - this.canvasMouseX;
                var canvasDeltaY = e.clientY - this.canvasMouseY;

                this.canvasMouseMoveX += canvasDeltaX;
                this.canvasMouseMoveY += canvasDeltaY;
            }

            this.canvasMouseX = e.clientX;
            this.canvasMouseY = e.clientY;
        });
        document.getElementById("m_Canvas").addEventListener("mouseout", () => {
            this.canvasClicked = false;
        });
        document.getElementById("m_Canvas").addEventListener("mouseup", () => {
            this.canvasClicked = false;
        });
    }

    /**
     * Initializes the WebGL2 context, creates the screen quad and loads
     * the default shaders.
     */
    init() {
        try {
            this.gl = this.canvas.getContext("webgl2");
        } catch (e) {}

        if (!this.gl) {
            window.alert("Error: Could not create WebGL2 Context");
            return;
        }

        this.createScreenQuad();
        this.setupShader();

        this.resize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    /**
     * Render loop (does actual drawing)
     *
     * @param {number} timestamp the time since 'origin'
     */
    display(timestamp) {

        this.fps.update(timestamp);

        this.gl.clearColor(...this.clearColor);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);

        this.gl.useProgram(this.progID);

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.bufID);

        // console.log(this.floor.displacement);
        // console.log(this.canvasPress0);
        // if(this.rolling)
        // {
        //     this.floor.displacement = Math.min(1.0, this.floor.displacement + 0.1);
        // }
        // else
        // {
        //     this.floor.displacement = Math.max(0.0, this.floor.displacement - 0.2);
        // }

        this.floor.step();

        if(this.dice.actualDiceNumber == 1)
        {
            this.dice.fall();
            console.log(this.dice.pos.y);
        }
        else if(this.dice.roll)
        {
            this.dice.step();
        }
            // console.log(this.floor.displacement);

        // set uniforms
        const w = this.canvas.width;
        const h = this.canvas.height;
        this.gl.uniform2f(this.uniforms["uResolution"], w, h);
        this.gl.uniform1f(this.uniforms["time"], timestamp);
        // Mouse events
        this.gl.uniform1i(this.uniforms["mouseClicked"], this.canvasClicked);
        this.gl.uniform2i(this.uniforms["deltaPos"], this.canvasMouseMoveX, this.canvasMouseMoveY);
        // floor displacement
        this.gl.uniform1f(this.uniforms["uDisplacementSpeed"], this.floor.displacement );
        this.gl.uniform3f(this.uniforms["uFloorColor"], this.floor.color1.x, this.floor.color1.y, this.floor.color1.z );

        // Dice Attributes
        this.gl.uniform3f(this.uniforms["uDicePos"], this.dice.pos.x, this.dice.pos.y, this.dice.pos.z);
        this.gl.uniform3f(this.uniforms["uDiceRot"], this.dice.rot.x, this.dice.rot.y, this.dice.rot.z);
        this.gl.uniform1i(this.uniforms["uDiceNumber"], this.dice.actualDiceNumber);

        // Camera Attributes
        this.gl.uniform3f(this.uniforms["uCamPos"], this.dice.camPos.x, this.dice.camPos.y, this.dice.camPos.z );
        this.gl.uniform3f(this.uniforms["uCamDir"], this.dice.camDir.x, this.dice.camDir.y, this.dice.camDir.z );
        this.gl.uniform3f(this.uniforms["uCamUp"], this.cameraUp.x, this.cameraUp.y, this.cameraUp.z );

        // draw fullscreen quad (2 triangles)
        this.gl.drawArrays(this.gl.TRIANGLES, 0, 6);

        // request next frame
        window.requestAnimationFrame(this.display.bind(this));
    }

    /**
     * Resizes the canvas and viewport to the desired dimensions
     *
     * @param {number} w desired width
     * @param {number} h desired height
     */
    resize(w=screen.width, h=screen.height) {
        this.canvas.width = w;
        this.canvas.height = h;
        this.gl.viewport(0, 0, w, h);
    }

    /**
     * Toggle fullscreen mode, activated by:
     * - pressing f,
     * - clicking the fullscreen button
     */
    toggleFullscreen() {
        if (!document.fullscreenEnabled) {
            alert("This document does not support fullscreen!");
            return;
        }

        if (!document.fullscreenElement) {
            this.parent
                .requestFullscreen()
                .catch(() => {
                    console.log("Sorry, could not enter fullscreen =(");
                });
        } else {
            document
                .exitFullscreen()
                .catch(() => {
                    console.log("Your are now trapped");
                });
        }
    }

    moveAndRotateCamera( move_x=0, move_z=0, rotate_h=0, rotate_v=0 ) {
        // The Look-At-Point
        var dPoint = new Point3D( 0 + this.cameraDir.x, 0 + this.cameraDir.y, 0 + this.cameraDir.z, 1 );
        // Position of the camera
        var pPoint = new Point3D( 0 + this.cameraPos.x, 0 + this.cameraPos.y, 0 + this.cameraPos.z, 1 );
        // The directional vector ( dPoint - pPoint)
        var move_dir = dPoint.subtract( pPoint );
        move_dir = move_dir.normalize();
        // the Right-Vector (for look-at transformations)
        var rVector = move_dir.cross( new Vector3D( this.cameraUp.x, this.cameraUp.y, this.cameraUp.z ) ).normalize();
        // Up Vector
        var uVec = rVector.cross( move_dir ).normalize();
        // the movement speeds
        var move_speed_x = move_x;
        var move_speed_z = move_z;
        var rot_angle_xz = -rotate_h/4;
        var rot_angle_yz = rotate_v/4;

        /////// Basic movement is handled here //////
        var moveVector = rVector.mul( move_speed_x ).add( move_dir.mul( move_speed_z ) );
        var moveMatrix = new Matrix4D(1, 0, 0, moveVector.x,
                                      0, 1, 0, moveVector.y,
                                      0, 0, 1, moveVector.z,
                                      0, 0, 0, 1);
        pPoint = moveMatrix.mulVec( pPoint );
        dPoint = moveMatrix.mulVec( dPoint );


        // the calculation of the rotation should be made here
        var ctr = Math.cos(rot_angle_yz);
        var str = Math.sin(rot_angle_yz);
        var ctu = Math.cos(rot_angle_xz);
        var stu = Math.sin(rot_angle_xz);
        var I_v = new Matrix4D(   ctr, 0,   0,   0,
                                  0,   ctr, 0,   0,
                                  0,   0,   ctr, 0,
                                  0,   0,   0,   1);
        var I_h = new Matrix4D(   ctu, 0,   0,   0,
                                  0,   ctu, 0,   0,
                                  0,   0,   ctu, 0,
                                  0,   0,   0,   1);
        var nnT_v = new Matrix4D( rVector.x*rVector.x, rVector.x*rVector.y, rVector.x*rVector.z,
                                  rVector.y*rVector.x, rVector.y*rVector.y, rVector.y*rVector.z,
                                  rVector.z*rVector.x, rVector.z*rVector.y, rVector.z*rVector.z,
                                  0, 0, 0, 1).mulScal(1-ctr);
        var nnT_h = new Matrix4D( uVec.x*uVec.x, uVec.x*uVec.y, uVec.x*uVec.z,
                                  uVec.y*uVec.x, uVec.y*uVec.y, uVec.y*uVec.z,
                                  uVec.z*uVec.x, uVec.z*uVec.y, uVec.z*uVec.z,
                                  0, 0, 0, 1).mulScal(1-ctu);
        var Xn_v = new Matrix4D( 0, -rVector.z, rVector.y, 0,
                                 rVector.z,  0, -rVector.x,
                                 -rVector.y, rVector.x, 0, 0,
                                 0, 0, 0, 1 ).mulScal(-str);
        var Xn_h = new Matrix4D( 0,        -uVec.z, uVec.y,  0,
                                 uVec.z,   0,       -uVec.x, 0,
                                 -uVec.y,  uVec.x,        0, 0,
                                 0, 0, 0, 1 ).mulScal(-stu);
        var rotMat_v = I_v.addMat( nnT_v.addMat( Xn_v ) );
        var rotMat_h = I_h.addMat( nnT_h.addMat( Xn_h ) );
        // the direction we are looking at will be rotated twice.
        // Matrix.mulVec makes dVector into a Point
        dPoint = rotMat_h.mulVec( dPoint );
        dPoint = rotMat_v.mulVec( dPoint );
        // we only rotate along the up-Vector-axis along the right-vector's axis
        uVec = rotMat_v.mulVec( new Point3D(uVec.x, uVec.y, uVec.z, 1) );

        // Update the pos, dir and up values
        this.cameraPos = new Vector3D( pPoint.x, pPoint.y, pPoint.z );
        this.cameraUp = new Vector3D( uVec.x, uVec.y, uVec.z );
        this.cameraDir = new Vector3D( dPoint.x, dPoint.y, dPoint.z );
    }

    /**
     * Switch the status of a pressed button from an event.
     * @param {string} event the key to be toggled
     */
    toggleButtonEvent( event ) {

        var move_speed_x = 0.0;
        var move_speed_z = 0.0;
        var rot_angle_xz = 0.0;
        var rot_angle_yz = 0.0;
        console.log( "The key pressed was:" + event + "!" );
        // console.log( event );
        switch( event ) {
            case "w":
                this.canvasPressW = !this.canvasPressW;
                move_speed_z = 1.0;
                break;
            case "a":
                this.canvasPressA = !this.canvasPressA;
                move_speed_x = -1.0;
                break;
            case "s":
                this.canvasPressS = !this.canvasPressS;
                move_speed_z = -1.0;
                break;
            case "d":
                this.canvasPressD = !this.canvasPressD;
                move_speed_x = 1.0;
                break;
            case "j":
                this.canvasPressJ = !this.canvasPressJ;
                rot_angle_xz = -1.0;
                break;
            case "k":
                this.canvasPressK = !this.canvasPressK;
                rot_angle_yz = -1.0;
                break;
            case "l":
                this.canvasPressL = !this.canvasPressL;
                rot_angle_xz = 1.0;
                break;
            case "i":
                this.canvasPressI = !this.canvasPressI;
                rot_angle_yz = 1.0;
                break;
            case "0":
                this.rolling = !this.rolling;
                if(this.rolling)
                {
                    this.dice.start();
                }
                else
                {
                    this.dice.stop();
                }
                // console.log("Start Dice with Speed = ( " + this.dice.speed.x + " , " + this.dice.speed.y + " , " + this.dice.speed.z + " ) and Rot = ( " + this.dice.rot.x + " , " + this.dice.rot.y + " , " + this.dice.rot.z + " )");
                break;
            case "1":
                if(this.rolling)
                {
                    this.dice.stop(1);
                    this.rolling = false;
                }
                break;
            case "2":
                if(this.rolling)
                {
                    this.dice.stop(2);
                    this.rolling = false;
                }
                break;
            case "3":
                if(this.rolling)
                {
                    this.dice.stop(3);
                    this.rolling = false;
                }
                break;
            case "4":
                if(this.rolling)
                {
                    this.dice.stop(4);
                    this.rolling = false;
                }
                break;
            case "5":
                if(this.rolling)
                {
                    this.dice.stop(5);
                    this.rolling = false;
                }
                break;
            case "6":
                if(this.rolling)
                {
                    this.dice.stop(6);
                    this.rolling = false;
                }
                break;
            case "7":
                this.canvasPress7 = !this.canvasPress7;
                break;
            case "8":
                this.canvasPress8 = !this.canvasPress8;
                break;
            case "9":
                this.canvasPress9 = !this.canvasPress9;
                break;
            case " ":
                this.dice.toggleRoll();
                break;
            default:
                break;
        }
        this.moveAndRotateCamera( move_speed_x, move_speed_z, rot_angle_xz, rot_angle_yz );
    }

    /**
     * Initializes buffer with a screen quad (position + color)
     *
     * @param {WebGL2RenderingContext} gl
     */
    createScreenQuad(gl=this.gl) {
        if (!this.bufID) {
            // generate a Vertex Buffer Object (VBO)
            this.bufID = gl.createBuffer();

            // 3 position, 4 color
            const triangleVertexData = new Float32Array([
                -1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0,
                -1.0,-1.0, 0.0, 0.0, 0.0, 0.0, 1.0,
                1.0,1.0, 0.0, 0.0, 0.0, 1.0, 1.0,
                -1.0, -1.0, 0.0, 0.0, 0.0, 0.0, 1.0,
                1.0,-1.0, 0.0, 0.0, 0.0, 0.0, 1.0,
                1.0,1.0, 0.0, 0.0, 0.0, 1.0, 1.0
            ]);

            gl.bindBuffer(gl.ARRAY_BUFFER, this.bufID);
            gl.bufferData(gl.ARRAY_BUFFER, triangleVertexData, gl.STATIC_DRAW);
        }
    }

    /**
     * Updates the shader source and deletes the old shader program
     *
     * @param {string} inp_vertSrc vertex shader source
     * @param {string} inp_fragSrc fragment shader source
     */
    updateShader(inp_vertSrc, inp_fragSrc) {
        this.vertSrc = inp_vertSrc;
        this.fragSrc = inp_fragSrc;

        this.gl.deleteProgram(this.progID);
        this.gl.deleteShader(this.vertID);
        this.gl.deleteShader(this.fragID);
    }

    /**
     * Sets up everything related to the shader (calls setupAttributes())
     * -> creates, compiles, attaches and links shader
     *
     * @param {WebGL2RenderingContext} gl
     */
    setupShader(gl=this.gl) {

        // create shader
        this.vertID = gl.createShader(gl.VERTEX_SHADER);
        this.fragID = gl.createShader(gl.FRAGMENT_SHADER);

        // specify shader source
        gl.shaderSource(this.vertID, this.vertSrc);
        gl.shaderSource(this.fragID, this.fragSrc);

        // compile the shader
        gl.compileShader(this.vertID);
        gl.compileShader(this.fragID);

        let error = false;
        // check vertex shader compilation status
        if (!gl.getShaderParameter(this.vertID, gl.COMPILE_STATUS)) {
            document.getElementById("vert_error").innerHTML = `invalid vertex shader :
                ${gl.getShaderInfoLog(this.vertID)}`;
            error = true;
        } else {
            document.getElementById("vert_error").innerHTML = "";
        }
        // check fragment shader compilation status
        if (!gl.getShaderParameter(this.fragID, gl.COMPILE_STATUS)) {
            document.getElementById("frag_error").innerHTML = `invalid fragment shader :
                ${gl.getShaderInfoLog(this.fragID)}`;
            error = true;
        } else {
            document.getElementById("frag_error").innerHTML = "";
        }

        if (error) {
            alert("Error: Could not compile shader");
            return;
        }

        // create program and attach shaders
        this.progID = gl.createProgram();
        gl.attachShader(this.progID, this.vertID);
        gl.attachShader(this.progID, this.fragID);

        // link the program
        gl.linkProgram(this.progID);
        if (!gl.getProgramParameter(this.progID, gl.LINK_STATUS)) {
            alert(gl.getProgramInfoLog(this.progID));
            return;
        }

        this.readUniformLocations(gl);
        this.setupAttributes(gl);
    }

    /**
     * Read and store uniform locations
     *
     * @param {WebGL2RenderingContext} gl
     */
    readUniformLocations(gl=this.gl) {
        // maybe store names in a map if there is more than 1 uniform
        const resolution = "uResolution";
        const time = "time";
        const mouseClicked = "mouseClicked";
        const deltaPos = "deltaPos";
        const camPos = "uCamPos";
        const camUp = "uCamUp";
        const camDir = "uCamDir";
        const dicePos = "uDicePos";
        const diceRot = "uDiceRot";
        const dispSpeed = "uDisplacementSpeed";
        const floorColor = "uFloorColor";
        const diceNumber = "uDiceNumber";

        this.uniforms[resolution] = gl.getUniformLocation(this.progID, resolution);
        this.uniforms[time] = gl.getUniformLocation(this.progID, time);
        this.uniforms[mouseClicked] = gl.getUniformLocation(this.progID, mouseClicked);
        this.uniforms[deltaPos] = gl.getUniformLocation(this.progID, deltaPos);
        this.uniforms[camPos] = gl.getUniformLocation( this.progID, camPos );
        this.uniforms[camDir] = gl.getUniformLocation( this.progID, camDir );
        this.uniforms[camUp] = gl.getUniformLocation( this.progID, camUp );
        this.uniforms[dicePos] = gl.getUniformLocation(this.progID, dicePos);
        this.uniforms[diceRot] = gl.getUniformLocation(this.progID, diceRot);
        this.uniforms[floorColor] = gl.getUniformLocation(this.progID, floorColor );
        this.uniforms[diceNumber] = gl.getUniformLocation(this.progID, diceNumber);


        this.uniforms[dispSpeed] = gl.getUniformLocation( this.progID, dispSpeed );
    }

    /**
     * Get attribute locations and enable vertex attribute arrays
     *
     * @param {WebGL2RenderingContext} gl
     */
    setupAttributes(gl=this.gl) {
        // "in_Position" and "in_Color" are user-provided
        // ATTRIBUTE variables of the vertex shader.
        // Their locations are stored to be used later with
        // glEnableVertexAttribArray()
        this.vertexLoc = gl.getAttribLocation(this.progID,"in_Position");
        this.color1Loc = gl.getAttribLocation(this.progID, "in_Color");

        // position
        let offset = 0;
        const stride = (3 + 4) * Float32Array.BYTES_PER_ELEMENT;
        gl.vertexAttribPointer(this.vertexLoc, 3, gl.FLOAT, false, stride, offset);
        gl.enableVertexAttribArray(this.vertexLoc);

        // color
        offset = 0 + 3 * Float32Array.BYTES_PER_ELEMENT;
        gl.vertexAttribPointer(this.color1Loc, 4, gl.FLOAT, false, stride, offset);
        gl.enableVertexAttribArray(this.color1Loc);
    }
}
