% Template Source: This template has been downloaded from:
% http://www.latextemplates.com/template/stylish-article
% License: CC BY-NC-SA 3.0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[english,fleqn,10pt]{SelfArx} % Document font size and equations flushed left
\usepackage[T1]{fontenc}
\usepackage{libertine}
\usepackage{sourcecodepro}
\usepackage{libertinust1math}
\usepackage{listings}
\usepackage{lipsum} % Required to insert dummy text. To be removed otherwise
\renewcommand{\vec}[1]{\ensuremath{\overrightarrow{#1}}}

%----------------------------------------------------------------------------------------
%	COLUMNS
%----------------------------------------------------------------------------------------

\setlength{\columnsep}{0.55cm} % Distance between the two columns of text
\setlength{\fboxrule}{0.75pt} % Width of the border around the abstract

%----------------------------------------------------------------------------------------
%	COLORS
%----------------------------------------------------------------------------------------

\definecolor{color1}{RGB}{0,120,80} % Color of the article title and sections
\definecolor{color2}{RGB}{9,47,86} % Color of the boxes behind the abstract and headings

%----------------------------------------------------------------------------------------
%	HYPERLINKS
%----------------------------------------------------------------------------------------

\usepackage{hyperref} % Required for hyperlinks
\hypersetup{hidelinks,colorlinks,breaklinks=true,urlcolor=color2,citecolor=color1,linkcolor=color1,bookmarksopen=false,pdftitle={Dice Simulation using shader-only rendering},pdfauthor={Carlos Alfonso Parra Fernandez}}

%----------------------------------------------------------------------------------------
%	ARTICLE INFORMATION
%----------------------------------------------------------------------------------------

\JournalInfo{Praktikum \emph{Computergrafik}, WS 2019/20} % Journal information
\Archive{Prof. Dr. Marcel Campen} % Additional notes (e.g. copyright, DOI, review/research article)

\PaperTitle{Report on using Shader-Only Rendering for simulating a dice} % Article title

\Authors{Carlos Alfonso Parra Fernandez\textsuperscript{1}} % Authors
\affiliation{\textsuperscript{1}\textit{Universität Osnabrück, Deutschland}} % Author affiliation

\Keywords{Subsurface Scattering --- Noise --- Terrain} % Keywords - if you don't want any simply remove all the text between the curly brackets
\newcommand{\keywordname}{Keywords} % Defines the keywords heading name

%----------------------------------------------------------------------------------------
%	ABSTRACT
%----------------------------------------------------------------------------------------

\Abstract{
          This report is going to explore the techniques used for the simulation of the terrain and the
          subsurface scattering factor of the lighting involved. Additionally, some noise-generation functions
          were considered, but were -- due to performance -- not implemented in the final product. \par
          In the final shader a dice will spring into action by pressing the "\texttt{0}" key to start bouncing.
          Another press of the same key, would start bringing the dice to a halt making it land on a random
          face. If instead one of the keys from "\texttt{1}" to "\texttt{6}" is pressed, the dice will then fall with the pressed number
          facing up, and a predetermined scene effect will come into play.
          }

%----------------------------------------------------------------------------------------

\begin{document}
\flushbottom % Makes all text pages the same height
\maketitle % Print the title and abstract box
\tableofcontents % Print the contents section
\thispagestyle{empty} % Removes page numbering from the first page
%----------------------------------------------------------------------------------------
%	ARTICLE CONTENTS
%----------------------------------------------------------------------------------------
\section*{Introduction} % The \section*{} command stops section numbering
\addcontentsline{toc}{section}{Introduction} % Adds this section to the table of contents
For the simulation of the light and the terrain, many steps were taken, and modularly applied to ensure the
extendability of the many aspects of lighting introduced.
Some of these are the subsurface scattering aspect of the lighting and the terrain generation.\par
Since transparency was implemented separately, the implementation put forth here will focus on the technical aspects of the illumination.
It will not be accounting for transparency within the ray-marching. The subsurface scattering is implemented in two different steps
that were better taken care off separately, since their respective implementations did not achieve a good enough
result on their own. The first part is the \textit{superficial scattering} i.e. light that wraps around the surface of an object,
whereas the second part focuses on the \textit{internal scattering} i.e. light that had entered and is now exiting the body of the object.\par
Concerning the terrain, the implementation of a mixture of sine and cosine functions takes place instead of an
actual noise function due to performance reasons
\footnote{The test laptop used for most of the coding and/or testing was taking over 10 minutes to compile code,
and the program itself was running at under 5 FPS:\\
% add Laptop Specs
Intel\textcopyright Core\texttrademark i5-3320M @ 2.6 GHz,
8GiB RAM,
Intel Integrated Graphics Controller @ 33MHz}.\par
The achieved result shows lit translucent objects that appear to be hollowed out objects with a thin
outer layer. This is completely due to the methods implemented. Regarding the terrain, the so-called
\textit{kkk-noise} is used, which turns out to be a performance-effective approach for uneven terrain generation,
even though an implementation of \textit{voronoi noise} is also readily available.
\begin{enumerate}[noitemsep]
    \item The floor disappears, the dice falls, then the scene resets.
    \item Water-like displacing surface.
    \item Cell-Shading for the whole scene.
    \item Dice becomes translucent.
    \item Dice becomes 100\% reflective.
    \item The color of the dices changes over time.
\end{enumerate}
A random option from the ones above will be chosen if the "\texttt{0}" key is pressed instead.
%------------------------------------------------
\section{Subsurface Scattering}
Before the first part of the subsurface scattering was implemented, only raw Phong-lighting was present (see left of
\textbf{Figure \ref{fig:scattering}}). When employing this, the sphere looks too artificially lit;
light gets unnaturally reflected and diffused, as if the surface of the object were completely solid and smooth.
In order to achieve a more natural, the light that wraps around the ball needs to be accounted for. In order to achieve this
the combination of two techniques --that are well established and very effective when it comes to replicating real-life subsurface scattering
satisfyingly-- is used.\par
Initially the terrain reflected a patterned floor made of entirely flat black and white tiles. This was changed to be more aesthetically pleasing 
dynamic approach:
during the simulation, the colors change each time the dice hits the floor.
The last bounces to avoid flickering lights. The noise function investigated and the one eventually implemented will be explained
in the following sections.
\begin{figure}[b]\centering % Using \begin{figure*} makes the figure take up the entire width of the page
    \includegraphics[width=\linewidth]{sss_super}
    \caption{Left -- Standard Phong illumination. Right -- Phong illumination and superficial subsurface scattering.}
    \label{fig:scattering}
\end{figure}
\subsection{Superficial Subsurface Scattering}
As mentioned previously, the \textit{raw} Phong illumination is not enough to give a natural feeling, when it comes to surfaces that 
display translucency (and even to opaque ones). In order to achieve a more natural look, the scattering is approximated through 
\textit{wrap lighting}. This allows the diffuse
factor from Phong's lighting model to expand past points where the normals are perpendicular to the light vector. This provides the
light with a way of \textit{reaching around} the object, even beyond the places where might have otherwise remained dark.
In order to achieve this wrapping, the implementation moves actively away from Phong's model towards the Nayar and Oren model, which
is better fit for rough and matte surfaces\cite[16.2]{GPU-Ge01}.\par
The basic concept behind this method is the following: since the diffuse factor $\delta$ ($\delta \geq 0$) is already present, 
the raw value is used in order to implement the \textit{wrapping} of the light. After calculating
the dot product between the light and surface normal the range of the diffuse factor can be expanded farther than the normal
perpendicular limit, but not too far, as to still leave place for shadows. Therefore the wrapping factor $\omega \in [0, 1]$ is used 
as seen in the \textbf{equation \ref{eq:wrapping}}. Having adjusted $\delta$ with some wrapping, there is another
small factor that needs to be accounted for: \textit{scattering}.
% Calculating diffuse + wrapping
\begin{equation}
    diffuse = max \{ 0, \frac{\delta + \omega}{1 + \omega} \}
    \label{eq:wrapping}
\end{equation}
In order to implement scattering, its concept must be first understood. Scattering is when light transitions after entering on a lit side to being in 
the shadow. \cite[16.1]{GPU-Ge01}.
In translucent bodies the light is absorbed on the side of the body that is facing the light, and comes out of another side which is in the shadow.
Some of this light is partially absorbed by the body, and the remainder is what can be seen coming out of the body. In the human body the remaining light tends to be
tainted red (due to our blood vessels and tissue). Regarding the object in the scene, this is just a color closer to the ambient colouring of the body. This transition
can easily be achieved using \textbf{equation \ref{eq:scattering}}, where \textit{s} stands for our scattering factor, \textit{d}
is the \textit{diffuse} factor obtained from \ref{eq:wrapping}, and \textit{w} is the desired width of the scattering.
% Calculating the scattering factor
\begin{equation}
    s = smoothstep(0, w, d) * smoothstep(w * 2, w, d)
    \label{eq:scattering}
\end{equation}
Lastly, \textit{diffuse} and \textit{s} need to be plugged into the lighting model\footnotemark; \textit{diffuse} has already
a place in the model, and \textit{s} is included by multiplying the \textit{ScatterColor} chosen with it. The result is then added to the final
diffuse factor, after accounting for the shadowing.
%Footnotes here
\footnotetext{The specular factor here need not be modified unlike in \cite[Example 16-1]{GPU-Ge01}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTERNAL SSS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Internal Subsurface Scattering}
By only implementing superficial scattering, light seems to just pass through our object (as seen in 
\textbf{Figure \ref{fig:translucency}}). The body does not absorb any of the light, nor are any of its
effects to be seen on the other side of the object. For this, the light travelling inside the shape must first:
\begin{enumerate}[noitemsep]
    \item be influenced by the varying thickness of the shape
    \item show some view \& light-dependent diffusion/attenuation
\end{enumerate}
Since actively calculating the distance travelled by light inside and object on a volumetrical manner
represents such a big performance cost here, the thickness is then semi-arbitrarily decided
based on the distance of the viewed point, and a the intersection of the light vector with an entry point
towards the object \footnote{average depth of an object at 
\textbf{p}, along the surface normal, based on a
            \textit{fallOff} coefficient\cite[thickness(...)]{ShTo-AlCo}}.\par
Now that the thickness of an object can be \textit{calculated} for a given point, it can be plugged into the code. 
Based on the lighting model seen in \textbf{Figure \ref{fig:sss-model}}, 
the following can be used\cite{GDC11}\par
\begin{itemize}[noitemsep]
    \item$fLightAttenuation = (\vec{N}*\vec{L})*(\vec{-L}*\vec{V})$
    \item$iLTPower = \tau*x$; with $x \geq 0$
    \item $fLTScale \in [0, 20]$
    \item$\vec{LTLight} = \vec{L}+\vec{N}*\textit{dist}$
    \item$fLTDot = saturate(\vec{V}*\vec{-LTLight} )^{iLTPower}*fLTScale$
    \item$\vec{fLT} = fLightAttenuation*(fLTDot+\alpha) * \textit{depth}$
    \item$\vec{absCol} = clamp[(\vec{fLT} * \delta * \vec{LightIntensity}), 0, 1]$
\end{itemize}
% Describe the variables
In which \vec{N}, \vec{L} \& \vec{V} represent the vectors from the former lighting model, 
$\tau$ represents the object transparency, which then scales \textit{iLTPower} with \textbf{\textit{x}}
\footnote{The bigger it gets, the more light will get absorbed and scattered within the object.}.
fLTScale describes the brightness of the light coming through and
LTLight the locally transmitted light
\footnote{which scales in accordance to a \textbf{\textit{dist}}ortion factor, that is surface dependent}.
$\alpha, \delta$ \& \textit{depth} stand for ambient, diffuse and local thickness respectively, while
absCol stands for the absorbed color which is the final value that is to be added at the end
of the lighting model. The results are then visible in \textbf{Figure \ref{fig:sss_full}}.
% Figure of the body with only translucency
\begin{figure}[b]\centering 
\includegraphics[width=\linewidth]{opac_only}
\caption{Translucent body without subsurface scattering}
\label{fig:translucency}
\end{figure}
% Fig 9 taken from the GDC2011 presentation
\begin{figure}[t]\centering
    \includegraphics[width=\linewidth]{sss-lighting-model}
    \caption{vector map for the lighting model from \cite[Fig. 9]{GDC11}}
    \label{fig:sss-model}
\end{figure}
% Transparent red square on floor
\begin{figure}[b]
    \includegraphics[width=\linewidth]{sss_full}
    \caption{The final  pass after both types of scattering have been applied}
    \label{fig:sss_full}
\end{figure}
%------------------------------------------------
\section{Voronoi Noise}
After touching onto the effects that are present on the surfaces of the objects in the scene,
an approach towards the workings of the noise on the terrain is also desired. The terrain 
goes from being a flat land, to becoming a very bumpy scenery --and vice versa-- upon pressing a button. 
This section will be much briefer than the previous one, since a much simpler
function --almost completely designed by Christoph Krämer-- was used, because of aforementioned performance issues.
The following explanation bases solely on \cite{IQ-ORG}\par

The presented work on the uneven nature of the floor is has its foundation on displacement operations used to simulate
watery surfaces, which is why the actual implementation only focuses on achieving the right amount and pattern of noise
for the creation of a convincing and satisfying terrain. 
The requirement for this are the coordinates of a point located at \vec{x} on the terrain 
\footnote{Its x and z coordinates,since the terrain is the grid represented on the plane x-z},
an amount of voronoi grid jittering $u \in [0,1]$ and an amount of interpolation between normal
noise and voronoi $v \in [0, 1]$. These parameters are then to be plugged into the algorithm seen on \textbf{Figure \ref{code:voronoi}}, and 
the height of the point on the terrain can then be easily obtained. That combined with the tiled colors of the terrain \textit{would} 
have given a satisfying result (as can see in \textbf{Figure \ref{fig:noisy-plane}}).
% The code in where the noise happens
\begin{figure}[bh]
    \includegraphics[width=\linewidth]{voronoi_noise}
    \caption{The two functions used to generate the height of the terrain (when using voronoi noise)}
    \label{code:voronoi}
\end{figure}

% what it could have been with voronoi noise
\begin{figure}[th]
    \includegraphics[width=\linewidth]{smooth_voronoi01}
    \caption{Capture of early footage of the implementation of voronoi noise. The framerate was so low despite only having to render the plane}
    \label{fig:noisy-plane}
\end{figure}
% We are on the final lap
\section{Results}
Regarding this project there are also other smaller aspects that are worth mentioning:
\begin{description}[noitemsep]
    \item [shadows] objects cast shadows of of different strength which also work along with their translucency
    \footnote{although the color of the shadows
    is not dependent on the color of the body casting them}.
    \item [translucency] the algorithm accounts for bodies that are also products of CSG. That means,
    that shapes within or behind other shapes are also able to be seen.
    \item [reflectiveness] the ray marching algorithm restarts based on a surface with a reflected vector. 
    \footnote{but reflections are only made once, since there is no recursion, and performance is what is important and since
    just one pass already does a good job}.
\end{description}

Considering the limited time spent working on the implementation and the recurrent performance issues, the final simulation shows satisfying results.
Regarding the more \textit{solid} aspects of the scene, some other operations also took place.
The CSG making the dice is created by combining a cube and 21 half-spheres, and the watery nature of the dice's surface (when it lands on a \texttt{2})
consists of the combined work of displacement\footnote{Using a 3D version of \textit{kkk-noise}.} and lighting. 
The combined work of the implementations can be reckoned in final product available here\ref{fig:final}.

\subsection{Discussion}
Since having focused mostly on the performance, the project had to be artificially shrunk down and limited.
This is not a spot of bother, thanks to the aesthetically pleasing minimalist nature of this easily expandable project.
The inner workings of the many aspects of the lighting involved acting
together all at the same time tend to sometimes cancel each other out,
and GLSL as a language can also be very unpredictable and inefficient specially when working and testing
with varied hardware --specially when it comes to the GPU--. On the other hand ray-marching is very accessible and the algorithms that are made
based on it can also be easily expandable (while still taking the limitations into account). The remaining is a project that can
indeed use some further optimization and simplification
\footnote{Since a lot of the performance on GLSL is lost in logical
branching and using operations outside of the basic ones, like 
\footnotesize{\texttt{pow(), normalize(), exp()}, etc }}, 
and re-touching, but the final product is very easily expandable. 

% The final product
\begin{figure*}[h]
    \includegraphics[width=\linewidth]{results}
    \caption{The basic dice, after starting the scene with \texttt{0}, and letting it bounce}
    \label{fig:final}
\end{figure*}




% \begin{table}[hbt]
% \caption{Table of Grades}
% \centering
% \begin{tabular}{llr}
% \toprule
% \multicolumn{2}{c}{Name} \par
% \cmidrule(r){1-2}
% First name & Last Name & Grade \par
% \midrule
% John & Doe & $7.5$ \par
% Richard & Miles & $2$ \par
% \bottomrule
% \end{tabular}
% \label{tab:label}
% \end{table}

% \begin{description}
% \item[Word] Definition
% \item[Concept] Explanation
% \item[Idea] Text
% \end{description}

% \begin{itemize}[noitemsep] % [noitemsep] removes whitespace between the items for a compact look
% \item First item in a list
% \item Second item in a list
% \item Third item in a list
% \end{itemize}


%------------------------------------------------

%----------------------------------------------------------------------------------------
%	REFERENCE LIST
%----------------------------------------------------------------------------------------
\phantomsection
\bibliographystyle{alpha}
\bibliography{sample}

%----------------------------------------------------------------------------------------

\end{document}